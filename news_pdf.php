<?php include('header.php'); ?>
<div class="not-home-page university-home container">
    <div class="">
        <div class="list-header">
            <h2 class="list-header__header">New cancer findings can give wider access to immunotherapy New cancer findings can give wider access to immunotherapy </h2>
             <a href="#" target="_blank" class="list-header__icon list-header__icon--rss list-header__item--mla">
                <span></span>
            </a>
        </div>
        <div class="news-datetime">
            <p>Published: 2021-07-06 09:05 | Updated: 2021-07-06 10:36</p>
        </div> 
<!--        <div>-->
<!--            <img src="images/banner/doctor-with-laptop.jpg" alt="">-->
<!--        </div>-->
        <div class="news-description">
            <p style="text-align: justify;">
                Researchers at Karolinska Institutet publish new findings in the journal Cancer Discovery showing how pharmacological activation of the protein p53 boosts the immune response against tumours. The results can be of significance to the development of new combination therapies that will give more cancer patients access to immunotherapy.
                <br>
                Given its ability to react to damage to cellular DNA and the key part it is thought to play in preventing tumour growth, the protein p53 has been dubbed the “guardian of the genome”. Half of all tumours have mutations in the gene that codes for the protein, and in many other tumours, p53 is  disabled by another protein, MDM2.
                <br><br>
                It has long been known that p53 is able to silence certain sequences in our genome called endogenous retroviruses (i.e. DNA elements evolutionarily inherited from viruses), thus preventing genome instability. The researchers now show that the protein can also activate these sequences in cancer cells, leading to anti-tumour immune response.
                <br><br>
            </p>
            <div class="pdf-file-content">
                <object data="docs/sample-pdf-file.pdf" type="application/pdf" width="100%" height="500">
                    alt : <a href="docs/sample-pdf-file.pdf">sample-pdf-file.pdf</a>
                </object>
            </div>
        </div>

    </div>

</div>
<?php include('footer.php'); ?>

<?php include('header.php'); ?>
<div class="not-home-page university-home container">
    <div class="vc-msg-area">
        <div class="list-header">
            <h2 class="list-header__header">Contact</h2>
            <a href="#" target="_blank" class="list-header__icon list-header__icon--rss list-header__item--mla">
                <span></span>
            </a>
        </div>
        <div class="row">
            <div class="col-6">
                <h4 class="py-5"><u>General Information</u> </h4>
                <p style="text-align: justify;">
                    <i style="font-size: 18px;width: 25px;text-align: center;" class="fa fa-map-o" aria-hidden="true"></i>
                    <b>Chittagong Medical University</b>
                    <br><br>
                    <i style="font-size: 18px;width: 25px;text-align: center;" class="fa fa-map-marker" aria-hidden="true"></i>
                     BITID Building, Faujdarhat. Chittagong, Bangladesh.
                    <br>
                    <i style="font-size: 18px;width: 25px;text-align: center;" class="fa fa-phone-square" aria-hidden="true"></i>
                    Telephone: 031-2780430, 031-2780428
                    <br>
                    <i style="font-size: 18px;width: 25px;text-align: center;" class="fa fa-envelope-o" aria-hidden="true"></i>
                    Email: info@cmu.edu.bd
                    <br>
                    <i style="font-size: 18px;width: 25px;text-align: center;" class="fa fa-globe" aria-hidden="true"></i>
                     Website: www.cmu.edu.bd
                </p>
            </div>
            <div class="col-6">
                <h4 class="py-5"><u>Liaison Office</u></h4>
                <p style="text-align: justify;">
                    <i style="font-size: 18px;width: 25px;text-align: center;" class="fa fa-map-signs" aria-hidden="true"></i> Nuclear Medicine Building (6th Floor), Dhaka Medical College Campus, Dhaka, Bangladesh.
                </p>
            </div>

            <div class="col-12 py-5">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3688.7713928429666!2d91.756107!3d22.399974!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30acd98ae9633061%3A0x7fb19033c7a3c849!2sFaujdarhat%2C%20Salimpur!5e0!3m2!1sen!2sbd!4v1625983900256!5m2!1sen!2sbd" width="100%" height="600" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>
        </div>

    </div>

</div>
<?php include('footer.php'); ?>

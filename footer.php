
<!-- Footer Start -->
<footer id="rs-footer" class="rs-footer rs-footer-style7 ">
    <div class="container">
        <!-- Footer Address -->
        <div>
            <div class="row footer-contact-desc">
                <div class="col-md-4">
                    <div class="contact-inner">
                        <i class="fa fa-map-marker"></i>
                        <h4 class="contact-title">Address</h4>
                        <p class="contact-desc">
                            BITID Building, Faujdarhat. Chittagong, Bangladesh.
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="contact-inner">
                        <i class="fa fa-phone"></i>
                        <h4 class="contact-title">Phone Number</h4>
                        <p class="contact-desc">
                            031-2780430<br> 031-2780428
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="contact-inner">
                        <i class="fa fa-envelope"></i>
                        <h4 class="contact-title">Email Address</h4>
                        <p class="contact-desc">
                            info@cmu.edu.bd
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- Footer Top -->
<div class="footer-top">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-12">
                <div class="about-widget">
                    <img src="images/CMU-Logo.png" alt="Logo">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed eius to mod tempor
                        incididunt ut labore et dolore magna
                        aliqua. Ut enims ad minim veniam.</p>
                    <p class="margin-remove">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed eius
                        to mod tempor incididunt ut labore et dolore magna
                        aliqua.</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-12">
                <h5 class="footer-title">RECENT POSTS</h5>
                <div class="recent-post-widget">
                    <div class="post-item">
                        <div class="post-date">
                            <span>28</span>
                            <span>June</span>
                        </div>
                        <div class="post-desc">
                            <h5 class="post-title"><a href="#">how all this idea</a></h5>
                        </div>
                    </div>
                    <div class="post-item">
                        <div class="post-date">
                            <span>28</span>
                            <span>June</span>
                        </div>
                        <div class="post-desc">
                            <h5 class="post-title"><a href="#">I must explain to you </a></h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-12">
                <h5 class="footer-title">OUR SITEMAP</h5>
                <ul class="sitemap-widget">
                    <li class="active"><a href="index.php"><i class="fa fa-angle-right" aria-hidden="true"></i>Home</a>
                    </li>
<!--                    <li><a href="about.html"><i class="fa fa-angle-right" aria-hidden="true"></i>About</a></li>-->
                    <li><a href="all-courses.php"><!--courses.html--><i class="fa fa-angle-right" aria-hidden="true"></i>Courses</a>
                    <li><a href="faculty.php"><i class="fa fa-angle-right" aria-hidden="true"></i>Faculty List</a>
                    </li>
<!--                    <li><a href="courses-details.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Courses-->
<!--                            Details</a></li>-->
                    <li><a href="all-notice.php"><i class="fa fa-angle-right" aria-hidden="true"></i>Events</a>
                    </li>
<!--                    <li><a href="events-details.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Events-->
<!--                            Details</a></li>-->
                </ul>
            </div>
            <div class="col-lg-3 col-md-12">
                <h5 class="footer-title">NEWSLETTER</h5>
                <p>Sign Up to Our Newsletter to Get Latest Updates &amp; Services</p>
                <div class="form-inner">
                    <input type="email" name="EMAIL" placeholder="Your email address" required="">
                    <input type="submit" value="Sign up">
                </div>
            </div>
        </div>
        <div class="footer-share">
            <ul>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                <li><a href="#"><i class="fa fa-vimeo"></i></a></li>
            </ul>
        </div>
    </div>
</div>

<!-- Footer Bottom -->
<div class="footer-bottom copyright_style7">
    <div class="copyright">
        <p>© 2021 All Rights Reserved. This webpage is made by <a href="http://cursorbd.com/" target="_blank">Cursor
                Limited</a>. </p>
    </div>
</div>
</footer>
<!-- Footer End -->

<!-- start scrollUp  -->
<div id="scrollUp">
    <i class="fa fa-angle-up"></i>
</div>

<!-- Canvas Menu start -->
<nav class="right_menu_togle" style="display: none;">
    <div class="close-btn"><span id="nav-close" class="text-center">x</span></div>
    <div class="canvas-logo">
        <a href="index.html"><img src="images/CMU-Logo.png" alt="logo"></a>
    </div>
    <ul class="sidebarnav_menu list-unstyled main-menu">
        <!--Home Menu Start-->
        <li class="current-menu-item menu-item-has-children"><a href="#">Home</a>

        </li>
        <!--Home Menu End-->

        <!--About Menu Start-->
        <li class="menu-item-has-children"><a href="#">About CMU</a>
            <ul class="list-unstyled">
                <li class="sub-nav active"><a href="#">Vice Chancellor's Message<span class="icon"></span></a></li>
                <li class="sub-nav"><a href="#">Vision, Mission & Objective<span class="icon"></span></a></li>
                <li class="sub-nav"><a href="#">Syndicate Members<span class="icon"></span></a></li>
                <li class="sub-nav"><a href="#">CMU Act<span class="icon"></span></a></li>
            </ul>
        </li>
        <!--About Menu End-->

        <!--Pages Menu Start-->
        <li class="menu-item-has-children"><a href="#">Academic</a>
            <ul class="list-unstyled">
                <li class="sub-nav active"><a href="#">Admission<span class="icon"></span></a></li>
                <li class="sub-nav"><a href="#">Academic Date<span class="icon"></span></a></li>
            </ul>
        </li>
        <!--Pages Menu End-->

        <!--Courses Menu Star-->
        <li class="menu-item-has-children"><a href="#">Courses</a>
            <ul class="list-unstyled">
                <li class="sub-nav"><a href="#">MBBS<span class="icon"></span></a></li>
                <li class="sub-nav"><a href="#">BDS<span class="icon"></span></a></li>
                <li class="sub-nav"><a href="#">Nursing<span class="icon"></span></a>
                <li class="sub-nav"><a href="#">Unani<span class="icon"></span></a>
                <li class="sub-nav"><a href="#">IMT<span class="icon"></span></a>
                <li class="sub-nav"><a href="#">Optometry<span class="icon"></span></a>
                </li>
            </ul>
        </li>
        <!--Courses Menu End-->

        <!--Events Menu Star-->
        <li class="menu-item-has-children"><a href="#">Others</a>
            <ul class="list-unstyled">
                <li class="sub-nav"><a href="#">Affiliated Institution<span class="icon"></span></a>
                    <ul class="list-unstyled">
                        <li class="sub-nav"><a href="#">Government<span class="icon"></span></a>
                        </li>
                        <li class="sub-nav"><a href="#">Non-Government<span class="icon"></span></a></li>
                    </ul>
                </li>
                <li class="sub-nav"><a href="#">Administration<span class="icon"></span></a>
                    <ul class="list-unstyled">
                        <li class="sub-nav"><a href="#">Chancellor<span class="icon"></span></a>
                        </li>
                        <li class="sub-nav"><a href="#">Vice Chancellor<span class="icon"></span></a></li>
                        <li class="sub-nav"><a href="#">Pro Vice Chancellor<span class="icon"></span></a></li>
                        <li class="sub-nav"><a href="#">Treasurer<span class="icon"></span></a></li>
                        <li class="sub-nav"><a href="#">Administrative Department<span class="icon"></span></a></li>
                    </ul>
                </li>
                <li class="sub-nav"><a href="#">Notice<span class="icon"></span></a>
                <li class="sub-nav"><a href="#">Research<span class="icon"></span></a>
            </ul>
        </li>
        <!--Events Menu End-->

        <!--blog Menu Star-->
        <li class="menu-item-has-children"><a href="#">Results</a>
        </li>
        <!--blog Menu End-->
        <li><a href="#">Contact<span class="icon"></span></a></li>
    </ul>
    <div class="search-wrap">
        <label class="screen-reader-text">Search for:</label>
        <input type="search" placeholder="Search..." name="s" class="search-input" value="">
        <button type="submit" value="Search"><i class="fa fa-search"></i></button>
    </div>
</nav>
<!-- Canvas Menu end -->

<!-- Search Modal Start -->
<div style="display: none;" aria-hidden="true" class="modal fade search-modal" role="dialog" tabindex="-1">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true" class="fa fa-close"></span>
    </button>
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="search-block clearfix">
                <form>
                    <div class="form-group">
                        <input class="form-control" placeholder="eg: Computer Technology" type="text">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Search Modal End -->

<!-- modernizr js -->
<script src="js/modernizr-2.8.3.min.js"></script>
<!-- jquery latest version -->
<script src="js/jquery.min.js"></script>
<!-- bootstrap js -->
<script src="js/bootstrap.min.js"></script>
<!-- owl.carousel js -->
<script src="js/owl.carousel.min.js"></script>
<!-- slick.min js -->
<script src="js/slick.min.js"></script>
<!-- isotope.pkgd.min js -->
<script src="js/isotope.pkgd.min.js"></script>
<!-- imagesloaded.pkgd.min js -->
<script src="js/imagesloaded.pkgd.min.js"></script>
<!-- wow js -->
<script src="js/wow.min.js"></script>
<!-- counter top js -->
<script src="js/waypoints.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>
<!-- magnific popup -->
<script src="js/jquery.magnific-popup.min.js"></script>
<!-- rsmenu js -->
<script src="js/rsmenu-main.js"></script>
<!-- Time Circle js -->
<script src="js/time-circle.js"></script>
<!-- plugins js -->
<script src="js/plugins.js"></script>
<!-- main js -->
<script src="js/main.js"></script>

<script>
    function menuOpen() {

        $('#parentDiv').slideToggle("slow");
        if($("#menu").attr('data-menu') == 1){
            $("#menu").attr('data-menu',2);
            $("#menu").html('CLOSE <i class="fa fa-times" aria-hidden="true"></i>');
            // $("#menu").text('')
        }else{
            $("#menu").attr('data-menu',1);
            $("#menu").html('MENU <i class="fa fa-bars" aria-hidden="true"></i>');
            // $("#menu").text('CLOSE')
        }
        //console.log('hello');
    }
</script>
<script>
    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    })
</script>
</body>

</html>
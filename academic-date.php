<?php include('header.php'); ?>
<div class="not-home-page university-home container">
    <div class="vc-msg-area">
        <div class="list-header">
            <h2 class="list-header__header">Academic Date</h2>
            <a href="#" target="_blank" class="list-header__icon list-header__icon--rss list-header__item--mla">
                <span></span>
            </a>
        </div>
        <div>
        </div>
        <div class="">
            <table class="table table-bordered"
                   style="border-top: 1px solid #dee2e6; border-bottom: 1px solid #dee2e6;">
                <thead>
                <tr>
                    <th><a style="color: #462461" href="#"><i
                                    class="fa fa-arrow-left"></i> Previous</a></th>
                    <th colspan="5" class="text-center">July 2021</th>
                    <th><a style="color: #462461" href="#"> Next <i
                                    class="fa fa-arrow-right"></i></a></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><strong>Sunday</strong></td>
                    <td><strong>Monday</strong></td>
                    <td><strong>Tuesday</strong></td>
                    <td><strong>Wednesday</strong></td>
                    <td><strong>Thrusday</strong></td>
                    <td><strong>Friday</strong></td>
                    <td><strong>Saturday</strong></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><p><strong>1</strong></p></td>
                    <td style="width: 15%;"><p><strong>2</strong></p></td>
                    <td style="width: 15%;"><p><strong>3</strong></p></td>
                </tr>
                <tr>
                    <td style="width: 15%;"><p><strong>4</strong></p></td>
                    <td style="width: 15%;"><p><strong>5</strong></p></td>
                    <td style="width: 15%;"><p><strong>6</strong></p></td>
                    <td style="width: 15%;"><p><strong>7</strong></p></td>
                    <td style="width: 15%;"><p><strong>8</strong></p></td>
                    <td style="width: 15%;"><p><strong>9</strong></p></td>
                    <td style="width: 15%;"><p><strong>10</strong></p></td>
                </tr>
                <tr>
                    <td style="width: 15%;"><p style="color:red"><strong>11</strong></p></td>
                    <td style="width: 15%;"><p><strong>12</strong></p></td>
                    <td style="width: 15%;"><p><strong>13</strong></p></td>
                    <td style="width: 15%;"><p><strong>14</strong></p></td>
                    <td style="width: 15%;"><p><strong>15</strong></p></td>
                    <td style="width: 15%;"><p><strong>16</strong></p></td>
                    <td style="width: 15%;"><p><strong>17</strong></p></td>
                </tr>
                <tr>
                    <td style="width: 15%;"><p><strong>18</strong></p></td>
                    <td style="width: 15%;"><p><strong>19</strong></p></td>
                    <td style="width: 15%;"><p><strong>20</strong></p></td>
                    <td style="width: 15%;"><p><strong>21</strong></p></td>
                    <td style="width: 15%;"><p><strong>22</strong></p></td>
                    <td style="width: 15%;"><p><strong>23</strong></p></td>
                    <td style="width: 15%;"><p><strong>24</strong></p></td>
                </tr>
                <tr>
                    <td style="width: 15%;"><p><strong>25</strong></p></td>
                    <td style="width: 15%;"><p><strong>26</strong></p></td>
                    <td style="width: 15%;"><p><strong>27</strong></p></td>
                    <td style="width: 15%;"><p><strong>28</strong></p></td>
                    <td style="width: 15%;"><p><strong>29</strong></p></td>
                    <td style="width: 15%;"><p><strong>30</strong></p></td>
                    <td style="width: 15%;"><p><strong>31</strong></p></td>
                </tr>
                </tbody>
            </table>

        </div>

    </div>

</div>
<?php include('footer.php'); ?>

<?php include('header.php'); ?>
<div class="not-home-page university-home container">
    <div class="">
        <div class="list-header">
            <h2 class="list-header__header">Results</h2>
            <a href="#" target="_blank" class="list-header__icon list-header__icon--rss list-header__item--mla">
                <span></span>
            </a>
        </div>

        <div class="row pt-5 pb-50">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                <table class="table table-striped">
                    <thead>
                    <tr><th class="text-center">SL</th>
                        <th class="text-center">Date</th>
                        <th class="text-center">Details</th></tr>
                    </thead>
                    <tbody><tr>
                        <td class="text-center">01</td>
                        <td class="text-center">12 Jan, 2020</td>
                        <td><a href="#" target="_blank">এম.বি.বি.এস নভেম্বর ২০১৯ পরীক্ষার ফলাফল প্রকাশ</a></td>
                    </tr>
                    <tr>
                        <td class="text-center">02</td>
                        <td class="text-center">12 Jan, 2020</td>
                        <td><a href="#" target="_blank">বি.ডি.এস নভেম্বর ২০১৯ পরীক্ষার ফলাফল প্রকাশ</a></td>
                    </tr>
                    <tr>
                        <td class="text-center">03</td>
                        <td class="text-center">12 Jan, 2020</td>
                        <td><a href="#" target="_blank">২য় বর্ষ পোস্ট বেসিক বিএসসি ইন পাবলিক হেলথ নার্সিং পরীক্ষা, জুলাই-২০১৯</a></td>
                    </tr>
                    <tr>
                        <td class="text-center">04</td>
                        <td class="text-center">12 Jan, 2020</td>
                        <td><a href="#" target="_blank">২য় বর্ষ পোস্ট বেসিক বিএসসি ইন নার্সিং পরীক্ষা, জুলাই-২০১৯</a></td>
                    </tr>
                    </tbody></table>
            </div>
        </div>

    </div>

</div>
<?php include('footer.php'); ?>

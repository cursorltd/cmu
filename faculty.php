<?php include('header.php'); ?>
<div class="not-home-page">
<!-- NEWS -->
<section class="paragraph paragraph--type--promos paragraph--view-mode--default layout--quintuple">
    <div class="container container--centered container--promos">
        <div class="list-header">
            <h2 class="list-header__header">Faculty List</h2>
<!--            <span class="list-header__label">The latest news from CMU</span>-->
<!--            <a href="#" target="_blank" class="list-header__icon list-header__icon--rss list-header__item--mla">-->
<!--                <span></span>-->
<!--            </a>-->
        </div>
        <div class="">
            <div class="collapse-area dept_list" id="faculty_list">
                <div class="card">
                    <div class="card-header" id="Anatomy">
                        <h3 class="m-0" data-toggle="collapse" data-target="#collapseAnatomy" aria-expanded="true" aria-controls="collapseAnatomy">
                            Anatomy
                        </h3>
                    </div>

                    <div id="collapseAnatomy" class="collapse show" aria-labelledby="Anatomy" data-parent="#faculty_list">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center" style="width: 10%;">SL.</th>
                                            <th class="text-center" style="width: 35%;">Name</th>
                                            <th class="text-center" style="width: 25%;">Degree</th>
                                            <th class="text-center" style="width: 30%;">Designation</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center">1.</td>
                                            <td>Faculty 01</td>
                                            <td class="text-center">MBBS,MD,FCPS</td>
                                            <td class="text-center">Head of the Department</td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">2.</td>
                                            <td>Faculty 02</td>
                                            <td class="text-center">MBBS</td>
                                            <td class="text-center">Professor</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="Biochemistry">
                        <h3 class="m-0 collapsed" data-toggle="collapse" data-target="#collapseBiochemistry" aria-expanded="false" aria-controls="collapseBiochemistry">
                            Biochemistry
                        </h3>
                    </div>
                    <div id="collapseBiochemistry" class="collapse" aria-labelledby="Biochemistry" data-parent="#faculty_list">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th class="text-center" style="width: 10%;">SL.</th>
                                        <th class="text-center" style="width: 35%;">Name</th>
                                        <th class="text-center" style="width: 25%;">Degree</th>
                                        <th class="text-center" style="width: 30%;">Designation</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="text-center">1.</td>
                                        <td>Faculty 01</td>
                                        <td class="text-center">MBBS,MD,FCPS</td>
                                        <td class="text-center">Head of the Department</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">2.</td>
                                        <td>Faculty 02</td>
                                        <td class="text-center">MBBS</td>
                                        <td class="text-center">Professor</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="Physiology">
                        <h3 class="m-0 collapsed" data-toggle="collapse" data-target="#collapsePhysiology" aria-expanded="false" aria-controls="collapsePhysiology">
                            Physiology
                        </h3>
                    </div>
                    <div id="collapsePhysiology" class="collapse" aria-labelledby="Physiology" data-parent="#faculty_list">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th class="text-center" style="width: 10%;">SL.</th>
                                        <th class="text-center" style="width: 35%;">Name</th>
                                        <th class="text-center" style="width: 25%;">Degree</th>
                                        <th class="text-center" style="width: 30%;">Designation</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="text-center">1.</td>
                                        <td>Faculty 01</td>
                                        <td class="text-center">MBBS,MD,FCPS</td>
                                        <td class="text-center">Head of the Department</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">2.</td>
                                        <td>Faculty 02</td>
                                        <td class="text-center">MBBS</td>
                                        <td class="text-center">Professor</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<!-- NEWS -->

</div>
<?php include('footer.php'); ?>

<?php include('header.php'); ?>
<div class="not-home-page">
<!-- NEWS -->
<section class="paragraph paragraph--type--promos paragraph--view-mode--default layout--quintuple">
    <div class="container container--centered container--promos">
        <div class="list-header">
            <h2 class="list-header__header">All Notice</h2>
            <span class="list-header__label">All notices from CMU</span>
            <a href="#" target="_blank" class="list-header__icon list-header__icon--rss list-header__item--mla">
                <span></span>
            </a>
        </div>
        <div class="">
            <table class="table">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Event</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>June 20, 2021</td>
                    <td>16:00</td>
                    <td>NOC for passport of Amir Ahmed, Store Keeper, Chittagong Medical University.</td>
                </tr>
                <tr>
                    <td>June 24, 2021 - June 22, 2021</td>
                    <td></td>
                    <td>1st year B.Sc Nursing (Post Basic),July 2018 Exam starts</td>
                </tr>
                <tr>
                    <td>June 26, 2021</td>
                    <td>10:30</td>
                    <td>Date Extension of Submission of Research Protocol.</td>
                </tr>
                <tr>
                    <td>June 24, 2021 - June 22, 2021</td>
                    <td></td>
                    <td>1st year B.Sc Nursing (Post Basic),July 2018 Exam starts</td>
                </tr>
                <tr>
                    <td>June 20, 2021</td>
                    <td>16:00</td>
                    <td>NOC for passport of Amir Ahmed, Store Keeper, Chittagong Medical University.</td>
                </tr>
                <tr>
                    <td>June 24, 2021 - June 22, 2021</td>
                    <td></td>
                    <td>1st year B.Sc Nursing (Post Basic),July 2018 Exam starts</td>
                </tr>
                <tr>
                    <td>June 26, 2021</td>
                    <td>10:30</td>
                    <td>Date Extension of Submission of Research Protocol.</td>
                </tr>
                <tr>
                    <td>June 20, 2021</td>
                    <td>16:00</td>
                    <td>NOC for passport of Amir Ahmed, Store Keeper, Chittagong Medical University.</td>
                </tr>
                <tr>
                    <td>June 26, 2021</td>
                    <td>10:30</td>
                    <td>Date Extension of Submission of Research Protocol.</td>
                </tr>
                </tbody>
            </table>
        </div>

    </div>
</section>
<!-- NEWS -->

</div>
<?php include('footer.php'); ?>

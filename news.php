<?php include('header.php'); ?>
<div class="not-home-page university-home container">
    <div class="">
        <div class="list-header">
            <h2 class="list-header__header">New cancer findings can give wider access to immunotherapy New cancer findings can give wider access to immunotherapy </h2>
             <a href="#" target="_blank" class="list-header__icon list-header__icon--rss list-header__item--mla">
                <span></span>
            </a>
        </div>
        <div class="news-datetime">
            <p>Published: 2021-07-06 09:05 | Updated: 2021-07-06 10:36</p>
        </div>
        <div>
            <img src="images/banner/doctor-with-laptop.jpg" alt="">
        </div>
        <div>
            <p style="text-align: justify;">
                Researchers at Karolinska Institutet publish new findings in the journal Cancer Discovery showing how pharmacological activation of the protein p53 boosts the immune response against tumours. The results can be of significance to the development of new combination therapies that will give more cancer patients access to immunotherapy.
                <br>
                Given its ability to react to damage to cellular DNA and the key part it is thought to play in preventing tumour growth, the protein p53 has been dubbed the “guardian of the genome”. Half of all tumours have mutations in the gene that codes for the protein, and in many other tumours, p53 is  disabled by another protein, MDM2.
                <br><br>
                It has long been known that p53 is able to silence certain sequences in our genome called endogenous retroviruses (i.e. DNA elements evolutionarily inherited from viruses), thus preventing genome instability. The researchers now show that the protein can also activate these sequences in cancer cells, leading to anti-tumour immune response.
                <br><br>
                Astonishing discovery<br>
                Galina Selivanova<br>
                Galina Selivanova. Photo: John Sennett<br>
                “This was an astonishing discovery. When we blocked the suppressor MDM2, p53 activated endogenous retroviruses which induced antiviral response and boosted the production of immune-activating interferons,” says lead investigator Galina Selivanova, Professor at the Department of Microbiology, Tumor and Cell Biology, Karolinska Institutet.
                <br><br>
                The results were obtained when the researchers blocked MDM2 in mouse models using a substance coded as ALRN-6924 from the US company Aileron Therapeutics. The increase of interferon response was also seen in tumour samples from two patients taking part in the company’s clinical trials of ALRN-6924.
                <br>
                There are synergies
                “This shows that there are synergies that should be exploited between substances that block MDM2 and modern immunotherapies,” Professor Selivanova continues. “A combination of these can be particularly important for patients who don’t respond to immunotherapy.”
                <br><br>
                Immunotherapy, or immuno-oncology, is described as a revolution in modern cancer treatment in the way it triggers the body’s immune system to fight cancer cells. However, it does not work with all patients, and the presence of interferons could be a biomarker for whether or not immunotherapy will prove efficacious.
                <br><br>
                “If we can increase the level of interferons, we can therefore increase the chances that the immunotherapy will succeed.”
                <br><br>
                Re-activating p53
                Professor Selivanova is a pioneer in research on how mutated p53 can be re-activated using special molecules, one of which, APR-246, is undergoing clinical studies under the name Eprenetapopt at Aprea Therapeutics, a company that she co-founded.
                <br><br>
                “We now want to examine if Eprenetapopt produces the same interferon boost and can have the same potential to increase access to immunotherapy for patients with severe forms of cancer,” she says.
                <br><br>
                The study was conducted at Karolinska Institutet and financed by the Swedish Cancer Society, the Swedish Research Council and Karolinska Institutet. Co-authors Vincent Guerlavais, Luis A. Carvajal, Manuel Aivado and D. Allen Annis are all employed by Aileron Therapeutics.
                <br><br>
                Publication
                “Pharmacological activation of p53 triggers viral mimicry response thereby abolishing tumor immune evasion and promoting anti-tumor immunity”. Xiaolei Zhou, Madhurendra Singh, Gema Sanz Santos, Vincent Guerlavais, Luis A. Carvajal, Manuel Aivado, Yue Zhan, Mariana M.S. Oliveira, Lisa S. Westerberg, D. Allen Annis, John Inge Johnsen and Galina Selivanova. Cancer Discovery, online 6 July 2021, doi: 10.1158/2159-8290.CD-20-1741.
                <br><br>
                TagsCancer and Oncology Drugs Immuno Therapy<br>
                Updated by:<br>
                Felicia Lindberg 2021-07-06<br>
                Share<br>
                Facebook<br>
                Twitter<br>
                LinkedIn<br>
                Related articles<br>
                Young boy in hospital bed<br>
                24 Jun 2021<br>
                <br><br>
                Hope for children with high-risk neuroblastoma<br>
                Thanks to many years of translational research, some children with the…<br>
                <br>
                2 Jun 2021
                <br>
                Analysis of 4,000 drugs reveals an alternative mechanism for a new anticancer therapy<br>
                By analyzing 4,000 drugs’ ability to affect cells’ capacity to produce…<br>
                <br>
                Hareth Nahi och Muhammad Kashif at the Deparment of medicine Huddinge<br>
                19 May 2021<br>
                <br>
                New findings provide hope for treatment of multiple myeloma<br>
                Researchers at Karolinska Institutet have investigated the use of low dose…<br>
                <br><br>
                Fingers holding the microneedle patch<br>
                10 May 2021<br>
                <br><br>
                Microneedle patch delivers antibiotics locally in the skin
                MRSA skin infections are often treated with intravenous injection of…
                <br><br>
                Illustration of DCIS
                5 May 2021
                <br><br>
                New marker predicts benefit of radiotherapy for early-stage breast cancer
                A study involving researchers at Karolinska Institutet and University of…
                <br><br>
                Navigate on the page
                New cancer findings can give wider access to immunotherapy
                <br><br>
            </p>
        </div>

    </div>

</div>
<?php include('footer.php'); ?>

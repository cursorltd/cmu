<?php include('header.php'); ?>
<div class="not-home-page">
<!-- NEWS -->
<section class="paragraph paragraph--type--promos paragraph--view-mode--default layout--quintuple">
    <div class="container container--centered container--promos">
        <div class="list-header">
            <h2 class="list-header__header">Research</h2>
            <span class="list-header__label">The latest research information from CMU</span>
            <a href="#" target="_blank" class="list-header__icon list-header__icon--rss list-header__item--mla">
                <span></span>
            </a>
        </div>
        <div class="promo-margin-container">
            <article class="promo-image promo type--regular with-link">
                <a class="fullsize with-image" href="research-details.php">
                    <span>Content_1</span>
                </a>
                <div class="inside with-image">
                    <div class="item__media">
                        <div class="flex-fix focalarea--mc"
                             style="background-image: url('images/home2-slider/1.jpg');">
                            <h2 class="linkhover"><span>Content_1</span>
                            </h2>
                        </div>
                    </div>
                    <div class="item__content">
                        <h2 class="has-link"><span>Content_1</span>
                        </h2>
                        <div class="content-area">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                Ipsum
                                has been the industry's standard dummy text ever since the 1500s, when an unknown
                                printer took a galley of type and scrambled it to make a type specimen book.</p>
                        </div>
                    </div>
                </div>
            </article>
            <article class="promo-image promo type--regular with-link">
                <a class="fullsize with-image" href="#">
                    <span>Content_2</span>
                </a>
                <div class="inside with-image">
                    <div class="item__media">
                        <div class="flex-fix focalarea--mr"
                             style="background-image: url('images/home2-slider/2.jpg');">
                            <h2 class="linkhover"><span>Content_2</span>
                            </h2>
                        </div>
                    </div>
                    <div class="item__content">
                        <h2 class="has-link"><span>Content_2</span>
                        </h2>
                        <div class="content-area">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                Ipsum
                                has been the industry's standard dummy text ever since the 1500s, when an unknown
                                printer took a galley of type and scrambled it to make a type specimen book
                            </p>
                        </div>
                    </div>
                </div>
            </article>
            <article class="promo-image promo type--regular with-link">
                <a class="fullsize with-image" href="#">
                    <span>Content_3</span>
                </a>
                <div class="inside with-image">
                    <div class="item__media">
                        <div class="flex-fix focalarea--mc"
                             style="background-image: url('images/home2-slider/3.jpg');">
                            <h2 class="linkhover"><span>Content_3</span>
                            </h2>
                        </div>
                    </div>
                    <div class="item__content">
                        <h2 class="has-link"><span>Content_3</span>
                        </h2>
                        <div class="content-area"></div>
                    </div>
                </div>
            </article>
            <article class="promo-image promo type--regular with-link">
                <a class="fullsize with-image" href="#">
                    <span>Content_4</span>
                </a>
                <div class="inside with-image">
                    <div class="item__media">
                        <div class="flex-fix focalarea--mc"
                             style="background-image: url('images/home2-slider/4.jpg');">
                            <h2 class="linkhover"><span>Content_4</span>
                            </h2>
                        </div>
                    </div>
                    <div class="item__content">
                        <h2 class="has-link"><span>Content_4</span>
                        </h2>
                        <div class="content-area">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                Ipsum
                                has been the industry's standard dummy text ever since the 1500s, when an unknown
                                printer took a galley of type and scrambled it to make a type specimen book.</p>
                        </div>
                    </div>
                </div>
            </article>
            <article class="promo-image promo type--regular with-link">
                <a class="fullsize with-image" href="#">
                    <span>Content_5</span>
                </a>
                <div class="inside with-image">
                    <div class="item__media">
                        <div class="flex-fix focalarea--mc"
                             style="background-image: url('images/home2-slider/5.jpg');">
                            <h2 class="linkhover"><span>Content_5</span>
                            </h2>
                        </div>
                    </div>
                    <div class="item__content">
                        <h2 class="has-link"><span>Content_5</span>
                        </h2>
                        <div class="content-area">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                Ipsum
                                has been the industry's standard dummy text ever since the 1500s, when an unknown
                                printer took a galley of type and scrambled it to make a type specimen book</p>
                        </div>
                    </div>
                </div>
            </article>
            <article class="promo-image promo type--regular with-link">
                <a class="fullsize with-image" href="news.php">
                    <span>Content_1</span>
                </a>
                <div class="inside with-image">
                    <div class="item__media">
                        <div class="flex-fix focalarea--mc"
                             style="background-image: url('images/home2-slider/1.jpg');">
                            <h2 class="linkhover"><span>Content_1</span>
                            </h2>
                        </div>
                    </div>
                    <div class="item__content">
                        <h2 class="has-link"><span>Content_1</span>
                        </h2>
                        <div class="content-area">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                Ipsum
                                has been the industry's standard dummy text ever since the 1500s, when an unknown
                                printer took a galley of type and scrambled it to make a type specimen book.</p>
                        </div>
                    </div>
                </div>
            </article>
            <article class="promo-image promo type--regular with-link">
                <a class="fullsize with-image" href="#">
                    <span>Content_2</span>
                </a>
                <div class="inside with-image">
                    <div class="item__media">
                        <div class="flex-fix focalarea--mr"
                             style="background-image: url('images/home2-slider/2.jpg');">
                            <h2 class="linkhover"><span>Content_2</span>
                            </h2>
                        </div>
                    </div>
                    <div class="item__content">
                        <h2 class="has-link"><span>Content_2</span>
                        </h2>
                        <div class="content-area">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                Ipsum
                                has been the industry's standard dummy text ever since the 1500s, when an unknown
                                printer took a galley of type and scrambled it to make a type specimen book
                            </p>
                        </div>
                    </div>
                </div>
            </article>
            <article class="promo-image promo type--regular with-link">
                <a class="fullsize with-image" href="#">
                    <span>Content_3</span>
                </a>
                <div class="inside with-image">
                    <div class="item__media">
                        <div class="flex-fix focalarea--mc"
                             style="background-image: url('images/home2-slider/3.jpg');">
                            <h2 class="linkhover"><span>Content_3</span>
                            </h2>
                        </div>
                    </div>
                    <div class="item__content">
                        <h2 class="has-link"><span>Content_3</span>
                        </h2>
                        <div class="content-area"></div>
                    </div>
                </div>
            </article>
            <article class="promo-image promo type--regular with-link">
                <a class="fullsize with-image" href="#">
                    <span>Content_4</span>
                </a>
                <div class="inside with-image">
                    <div class="item__media">
                        <div class="flex-fix focalarea--mc"
                             style="background-image: url('images/home2-slider/4.jpg');">
                            <h2 class="linkhover"><span>Content_4</span>
                            </h2>
                        </div>
                    </div>
                    <div class="item__content">
                        <h2 class="has-link"><span>Content_4</span>
                        </h2>
                        <div class="content-area">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                Ipsum
                                has been the industry's standard dummy text ever since the 1500s, when an unknown
                                printer took a galley of type and scrambled it to make a type specimen book.</p>
                        </div>
                    </div>
                </div>
            </article>
            <article class="promo-image promo type--regular with-link">
                <a class="fullsize with-image" href="#">
                    <span>Content_5</span>
                </a>
                <div class="inside with-image">
                    <div class="item__media">
                        <div class="flex-fix focalarea--mc"
                             style="background-image: url('images/home2-slider/5.jpg');">
                            <h2 class="linkhover"><span>Content_5</span>
                            </h2>
                        </div>
                    </div>
                    <div class="item__content">
                        <h2 class="has-link"><span>Content_5</span>
                        </h2>
                        <div class="content-area">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                Ipsum
                                has been the industry's standard dummy text ever since the 1500s, when an unknown
                                printer took a galley of type and scrambled it to make a type specimen book</p>
                        </div>
                    </div>
                </div>
            </article>
            <article class="promo-image promo type--regular with-link">
                <a class="fullsize with-image" href="news.php">
                    <span>Content_1</span>
                </a>
                <div class="inside with-image">
                    <div class="item__media">
                        <div class="flex-fix focalarea--mc"
                             style="background-image: url('images/home2-slider/1.jpg');">
                            <h2 class="linkhover"><span>Content_1</span>
                            </h2>
                        </div>
                    </div>
                    <div class="item__content">
                        <h2 class="has-link"><span>Content_1</span>
                        </h2>
                        <div class="content-area">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                Ipsum
                                has been the industry's standard dummy text ever since the 1500s, when an unknown
                                printer took a galley of type and scrambled it to make a type specimen book.</p>
                        </div>
                    </div>
                </div>
            </article>
            <article class="promo-image promo type--regular with-link">
                <a class="fullsize with-image" href="#">
                    <span>Content_2</span>
                </a>
                <div class="inside with-image">
                    <div class="item__media">
                        <div class="flex-fix focalarea--mr"
                             style="background-image: url('images/home2-slider/2.jpg');">
                            <h2 class="linkhover"><span>Content_2</span>
                            </h2>
                        </div>
                    </div>
                    <div class="item__content">
                        <h2 class="has-link"><span>Content_2</span>
                        </h2>
                        <div class="content-area">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                Ipsum
                                has been the industry's standard dummy text ever since the 1500s, when an unknown
                                printer took a galley of type and scrambled it to make a type specimen book
                            </p>
                        </div>
                    </div>
                </div>
            </article>
            <article class="promo-image promo type--regular with-link">
                <a class="fullsize with-image" href="#">
                    <span>Content_3</span>
                </a>
                <div class="inside with-image">
                    <div class="item__media">
                        <div class="flex-fix focalarea--mc"
                             style="background-image: url('images/home2-slider/3.jpg');">
                            <h2 class="linkhover"><span>Content_3</span>
                            </h2>
                        </div>
                    </div>
                    <div class="item__content">
                        <h2 class="has-link"><span>Content_3</span>
                        </h2>
                        <div class="content-area"></div>
                    </div>
                </div>
            </article>
            <article class="promo-image promo type--regular with-link">
                <a class="fullsize with-image" href="#">
                    <span>Content_4</span>
                </a>
                <div class="inside with-image">
                    <div class="item__media">
                        <div class="flex-fix focalarea--mc"
                             style="background-image: url('images/home2-slider/4.jpg');">
                            <h2 class="linkhover"><span>Content_4</span>
                            </h2>
                        </div>
                    </div>
                    <div class="item__content">
                        <h2 class="has-link"><span>Content_4</span>
                        </h2>
                        <div class="content-area">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                Ipsum
                                has been the industry's standard dummy text ever since the 1500s, when an unknown
                                printer took a galley of type and scrambled it to make a type specimen book.</p>
                        </div>
                    </div>
                </div>
            </article>
            <article class="promo-image promo type--regular with-link">
                <a class="fullsize with-image" href="#">
                    <span>Content_5</span>
                </a>
                <div class="inside with-image">
                    <div class="item__media">
                        <div class="flex-fix focalarea--mc"
                             style="background-image: url('images/home2-slider/5.jpg');">
                            <h2 class="linkhover"><span>Content_5</span>
                            </h2>
                        </div>
                    </div>
                    <div class="item__content">
                        <h2 class="has-link"><span>Content_5</span>
                        </h2>
                        <div class="content-area">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                Ipsum
                                has been the industry's standard dummy text ever since the 1500s, when an unknown
                                printer took a galley of type and scrambled it to make a type specimen book</p>
                        </div>
                    </div>
                </div>
            </article>
        </div>

        <div class="text-center">
            <a href="all-news.php" target="_blank" class="readon2">Load More News</a>
        </div>
    </div>
</section>
<!-- NEWS -->

</div>
<?php include('footer.php'); ?>

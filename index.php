<?php include('header.php'); ?>
    <div class="rs-banner-section2">
        <!-- image must be 1920*900 -->
        <!--        <img src="images/banner/home-banner.jpg" alt="" />-->
        <img src="images/banner/doctor-with-laptop.jpg" alt=""/>
        <!-- image must be 1920*900 -->
        <div class="banner-inner">
            <div class="container">
                <div class="banner-text">
                    <div class="sl-sub-title banner-text-bg">Vice Chacellor of CMU has been conferred as
                        <h4 class="banner-title"><span>"Visiting Professor"</span></h4>
                        by CyberJaya University of Medical Science, Malaysia. University.
                    </div>
                    <div class="btn-area pt-20">
                        <a href="#" class="readon">Read More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="animate">
                <div class="circle1">
                    <img src="images/circle.png" alt="">
                </div>
                <div class="circle2">
                    <img src="images/circle.png" alt="">
                </div>
            </div> -->
    <!--    </div>-->
    <!--Banner Section End-->

    <!-- NEWS -->
    <section class="paragraph paragraph--type--promos paragraph--view-mode--default layout--quintuple sec-padding-50">
        <div class="container container--centered container--promos">
            <div class="promo-margin-container">
                <article class="promo-image promo type--regular with-link">
                    <a class="fullsize with-image" href="#">
                        <span>Content_1</span>
                    </a>
                    <div class="inside with-image">
                        <div class="item__media">
                            <div class="flex-fix focalarea--mc"
                                 style="background-image: url('images/home2-slider/1.jpg');">
                                <h2 class="linkhover"><span>Content_1</span>
                                </h2>
                            </div>
                        </div>
                        <div class="item__content">
                            <h2 class="has-link"><span>Content_1</span>
                            </h2>
                            <div class="content-area">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                    Ipsum
                                    has been the industry's standard dummy text ever since the 1500s, when an unknown
                                    printer took a galley of type and scrambled it to make a type specimen book.</p>
                            </div>
                        </div>
                    </div>
                </article>
                <article class="promo-image promo type--regular with-link">
                    <a class="fullsize with-image" href="#">
                        <span>Content_2</span>
                    </a>
                    <div class="inside with-image">
                        <div class="item__media">
                            <div class="flex-fix focalarea--mr"
                                 style="background-image: url('images/home2-slider/2.jpg');">
                                <h2 class="linkhover"><span>Content_2</span>
                                </h2>
                            </div>
                        </div>
                        <div class="item__content">
                            <h2 class="has-link"><span>Content_2</span>
                            </h2>
                            <div class="content-area">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                    Ipsum
                                    has been the industry's standard dummy text ever since the 1500s, when an unknown
                                    printer took a galley of type and scrambled it to make a type specimen book
                                </p>
                            </div>
                        </div>
                    </div>
                </article>
                <article class="promo-image promo type--regular with-link">
                    <a class="fullsize with-image" href="#">
                        <span>Content_3</span>
                    </a>
                    <div class="inside with-image">
                        <div class="item__media">
                            <div class="flex-fix focalarea--mc"
                                 style="background-image: url('images/home2-slider/3.jpg');">
                                <h2 class="linkhover"><span>Content_3</span>
                                </h2>
                            </div>
                        </div>
                        <div class="item__content">
                            <h2 class="has-link"><span>Content_3</span>
                            </h2>
                            <div class="content-area"></div>
                        </div>
                    </div>
                </article>
                <article class="promo-image promo type--regular with-link">
                    <a class="fullsize with-image" href="#">
                        <span>Content_4</span>
                    </a>
                    <div class="inside with-image">
                        <div class="item__media">
                            <div class="flex-fix focalarea--mc"
                                 style="background-image: url('images/home2-slider/4.jpg');">
                                <h2 class="linkhover"><span>Content_4</span>
                                </h2>
                            </div>
                        </div>
                        <div class="item__content">
                            <h2 class="has-link"><span>Content_4</span>
                            </h2>
                            <div class="content-area">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                    Ipsum
                                    has been the industry's standard dummy text ever since the 1500s, when an unknown
                                    printer took a galley of type and scrambled it to make a type specimen book.</p>
                            </div>
                        </div>
                    </div>
                </article>
                <article class="promo-image promo type--regular with-link">
                    <a class="fullsize with-image" href="#">
                        <span>Content_5</span>
                    </a>
                    <div class="inside with-image">
                        <div class="item__media">
                            <div class="flex-fix focalarea--mc"
                                 style="background-image: url('images/home2-slider/5.jpg');">
                                <h2 class="linkhover"><span>Content_5</span>
                                </h2>
                            </div>
                        </div>
                        <div class="item__content">
                            <h2 class="has-link"><span>Content_5</span>
                            </h2>
                            <div class="content-area">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                    Ipsum
                                    has been the industry's standard dummy text ever since the 1500s, when an unknown
                                    printer took a galley of type and scrambled it to make a type specimen book</p>
                            </div>
                        </div>
                    </div>
                </article>
            </div>

            <div class="text-center">
                <a href="all-news.php" target="_blank" class="readon2">More Content</a>
            </div>
        </div>
    </section>
    <!-- NEWS -->

    <div class="paragraph paragraph--type--block paragraph--view-mode--default sec-padding-50">

        <div class="front-news-block">
            <div class="container container--centered">
                <div class="list-header">
                    <h2 class="list-header__header">News</h2>
                    <span class="list-header__label">The latest news from CMU</span>
                    <a href="#" target="_blank" class="list-header__icon list-header__icon--rss list-header__item--mla">
                        <span></span>
                    </a>
                </div>
                <div id="app">
                    <div class="front-news-block__container">
                        <div class="first">
                            <a href="#" class="with-image">
                                <div class="front-news-block__image"
                                     style="background-image: url('images/home2-slider/3.jpg');">
                                </div>
                                <div class="front-news-block__text-wrapper">
                                    <h3 class="front-news-block__title">First months decisive for immune system
                                        development
                                    </h3>
                                    <div class="front-news-block__created"><span class="icon"></span>
                                        17 June 2019
                                    </div>
                                    <div class="front-news-block__lead">Lorem Ipsum is simply dummy text of the printing
                                        and
                                        typesetting industry. Lorem Ipsum
                                        has been the industry's standard dummy text ever since the 1500s, when an
                                        unknown
                                        printer took a galley of type and scrambled it to make a type specimen book.
                                        <br>
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                        Ipsum
                                        has been the industry's standard dummy text ever since the 1500s, when an
                                        unknown
                                        printer took a galley of type and scrambled it to make a type specimen book.
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="second">
                            <a href="#" class="with-image"
                               style="background-image: url('images/home2-slider/2.jpg');">
                                <div class="front-news-block__image"></div>
                                <h3 class="front-news-block__title">Low income increases risk of recurrent heart attacks
                                </h3>
                                <!---->
                                <!---->
                            </a>
                            <a href="#" class="with-image"
                               style="background-image: url('images/home2-slider/4.jpg');">
                                <div class="front-news-block__image"></div>
                                <h3 class="front-news-block__title">Common vaccine protects against more HPV viruses
                                    than
                                    previously known</h3>
                                <!---->
                                <!---->
                            </a>
                        </div>
                        <div class="third">
                            <a href="#" class="with-image"
                               style="background-image: url('images/home2-slider/5.jpg');">
                                <div class="front-news-block__image"></div>
                                <h3 class="front-news-block__title">Immediate skin-to-skin contact after birth improves
                                    survival of pre-term babies</h3>
                                <!---->
                                <!---->
                            </a>
                            <a href="#" class="with-image"
                               style="background-image: url('images/home2-slider/1.jpg');">
                                <div class="front-news-block__image"></div>
                                <h3 class="front-news-block__title">Weight-loss treatment prevents accumulation of lipid
                                    linked to cardiac mortality</h3>
                                <!---->
                                <!---->
                            </a>
                        </div>
                    </div>
                    <!---->
                    <div class="text-center">
                        <a href="all-news.php" target="_blank" class="readon2">More Content</a>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <!-- notice-event -->
    <section class="notice-event-section rs-team-style7 sec-padding-50">
        <div class="container">
            <div class="list-header">
                <h2 class="list-header__header">Lectures and conferences</h2>
                <span class="list-header__label"></span>
                <a href="#" target="_blank" class="list-header__icon list-header__icon--rss list-header__item--mla">
                    <span></span>
                </a>
            </div>
            <!--            <div class="sec-title2 mb-50 text-center">-->
            <!--                <span class="primary-color">Lectures and conferences</span>-->
            <!--            <h2>We Provide Best Courses</h2>-->
            <!--            </div>-->
            <div class="row">
                <div class="col-lg-12">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Event</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>June 20, 2021</td>
                            <td>16:00</td>
                            <td>NOC for passport of Amir Ahmed, Store Keeper, Chittagong Medical University.</td>
                        </tr>
                        <tr>
                            <td>June 24, 2021 - June 22, 2021</td>
                            <td></td>
                            <td>1st year B.Sc Nursing (Post Basic),July 2018 Exam starts</td>
                        </tr>
                        <tr>
                            <td>June 26, 2021</td>
                            <td>10:30</td>
                            <td>Date Extension of Submission of Research Protocol.</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="text-center">
                <a href="all-notice.php" target="_blank" class="readon2">More Content</a>
            </div>
        </div>
    </section>
    <!-- notice-event -->

    <!-- Services Start -->
    <div class="rs-services rs-services-style7 pt-100 pb-100 d-none">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-7">
                    <div class="row pr-30 md-pr-0">
                        <div class="col-lg-6 col-md-6">
                            <div class="Services-wrap mb-30">
                                <div class="Services-item">
                                    <div class="Services-icon">
                                        <img src="images/services/7.jpg" alt="">
                                    </div>
                                    <div class="Services-desc">
                                        <i class="flaticon-book-1 mb-15"></i>
                                        <h4 class="services-title">
                                            <a href="#">Trending Courses</a>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                            <div class="Services-wrap">
                                <div class="Services-item">
                                    <div class="Services-icon">
                                        <img src="images/services/9.jpg" alt="">
                                    </div>
                                    <div class="Services-desc">
                                        <i class="flaticon-book mb-15"></i>
                                        <h4 class="services-title">
                                            <a href="#">Certified Teachers</a>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 pt-30 col-md-6">
                            <div class="Services-wrap mb-30">
                                <div class="Services-item">
                                    <div class="Services-icon">
                                        <img src="images/services/8.jpg" alt="">
                                    </div>
                                    <div class="Services-desc">
                                        <i class="flaticon-book mb-15"></i>
                                        <h4 class="services-title">
                                            <a href="#">Books & Library</a>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                            <div class="Services-wrap">
                                <div class="Services-item">
                                    <div class="Services-icon">
                                        <img src="images/services/10.jpg" alt="">
                                    </div>
                                    <div class="Services-desc">
                                        <i class="flaticon-tool-2"></i>
                                        <h4 class="services-title mb-15">
                                            <a href="#">Certification</a>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 pl-15 pt-30">
                    <div class="content-part">
                        <span class="sub-text">About Us</span>
                        <h2 class="uppercase title pb-20 md-pb-10">Welcome To Eshkool</h2>
                        <p class="pb-20">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed eius to mod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enims ad minim veniam. Aenean massa.
                            Cum sociis natoque penatibus et magnis dis partu rient to montes.Aene an massa. Cum sociis
                            natoque penatibus. Ut enims ad minim veniam.</p>

                        <p class="mb-30">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed eius to mod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enims ad minim veniam.</p>
                        <div class="btn-part">
                            <a class="readon2" href="#"> Read More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Services End -->

    <!-- Counter Up Section Start-->
    <div class="rs-counter-style7 pt-100 pb-100 bg5 d-none">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="rs-counter-list">
                        <div class="icon-part">
                            <i class="flaticon-tool"></i>
                        </div>
                        <div class="text-part">
                            <h2 class="counter-number plus">60</h2>
                            <h4 class="counter-desc">TEACHERS</h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="rs-counter-list">
                        <div class="icon-part">
                            <i class="flaticon-tool-2"></i>
                        </div>
                        <div class="text-part">
                            <h2 class="counter-number plus">40</h2>
                            <h4 class="counter-desc">COURSES</h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="rs-counter-list">
                        <div class="icon-part">
                            <i class="flaticon-book"></i>
                        </div>
                        <div class="text-part">
                            <h2 class="counter-number plus">900</h2>
                            <h4 class="counter-desc">STUDENTS</h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="rs-counter-list">
                        <div class="icon-part">
                            <i class="flaticon-book-1"></i>
                        </div>
                        <div class="text-part">
                            <h2 class="counter-number plus">3675</h2>
                            <h4 class="counter-desc">PUBLICATIONS</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Counter Down Section End -->

    <!-- Course Categories Start-->
    <div id="rs-courses-style7" class="rs-courses-style7 pt-90 pb-100 d-none">
        <div class="container">
            <div class="sec-title2 mb-45 text-center">
                <span class="primary-color">Course Categories</span>
                <h2> DESCOVER OUR CATEGORIES</h2>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="rs-carousel owl-carousel" data-loop="true" data-items="3" data-margin="20"
                         data-autoplay="false" data-autoplay-timeout="5000" data-smart-speed="1200" data-dots="true"
                         data-nav="true" data-nav-speed="false" data-mobile-device="1" data-mobile-device-nav="true"
                         data-mobile-device-dots="true" data-ipad-device="2" data-ipad-device-nav="true"
                         data-ipad-device-dots="true" data-md-device="3" data-md-device-nav="true"
                         data-md-device-dots="true">
                        <div class="item">
                            <div class="cource-img">
                                <!-- image size 520*395 -->
                                <img src="images/courses/style7/1.jpg" alt="">
                                <!-- image size 520*395 -->
                            </div>
                            <div class="content-part">
                                <div class="btn-part">
                                    <i class="flaticon-tool-2"></i>
                                </div>
                                <h4 class="cource-title">
                                    <a href="#">MBBS</a>
                                </h4>
                                <span class="courses-sub">O COURSES</span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="cource-img">
                                <img src="images/courses/style7/2.jpg" alt="">
                            </div>
                            <div class="content-part">
                                <div class="btn-part">
                                    <i class="flaticon-ribbon"></i>
                                </div>
                                <h4 class="cource-title">
                                    <a href="#">BDS</a>
                                </h4>
                                <span class="courses-sub">O COURSES</span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="cource-img">
                                <img src="images/courses/style7/3.jpg" alt="">
                            </div>
                            <div class="content-part">
                                <div class="btn-part">
                                    <i class="flaticon-graduation"></i>
                                </div>
                                <h4 class="cource-title">
                                    <a href="#">Nursing</a>
                                </h4>
                                <span class="courses-sub">1 COURSES</span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="cource-img">
                                <img src="images/courses/style7/4.jpg" alt="">
                            </div>
                            <div class="content-part">
                                <div class="btn-part">
                                    <i class="flaticon-book-1"></i>
                                </div>
                                <h4 class="cource-title">
                                    <a href="#">Unani</a>
                                </h4>
                                <span class="courses-sub">O COURSES</span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="cource-img">
                                <img src="images/courses/style7/5.jpg" alt="">
                            </div>
                            <div class="content-part">
                                <div class="btn-part">
                                    <i class="flaticon-book"></i>
                                </div>
                                <h4 class="cource-title">
                                    <a href="#">IMT</a>
                                </h4>
                                <span class="courses-sub">1 COURSES</span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="cource-img">
                                <img src="images/courses/style7/6.jpg" alt="">
                            </div>
                            <div class="content-part">
                                <div class="btn-part">
                                    <i class="flaticon-diploma"></i>
                                </div>
                                <h4 class="cource-title">
                                    <a href="#">Optometry</a>
                                </h4>
                                <span class="courses-sub">O COURSES</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="btn-part text-center">
                        <a class="readon2" href="#">View All Categories</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Course Categories End -->

    <!-- Courses Start -->
    <div id="rs-courses" class="rs-courses rs-courses-style6 sec-color sec-spacer d-none">
        <div class="container">
            <div class="sec-title2 mb-50 text-center">
                <span class="primary-color">Latest Course</span>
                <h2>We Provide Best Courses</h2>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="cource-item">
                        <div class="cource-img">
                            <img src="images/courses/6.jpg" alt="">
                            <a class="image-link" href="courses-details.html" title="University Tour 2018">
                                <i class="fa fa-link"></i>
                            </a>
                            <span class="course-value">Free</span>
                            <span class="img-part"><img src="images/team/1.jpg" alt=""></span>
                        </div>
                        <div class="course-body">
                            <span><a href="#">MBBS</a></span>
                            <h4 class="title"><a href="courses-details.html">MBBS</a></h4>

                        </div>
                        <div class="course-footer">
                            <div class="courses-seats">
                                <i class="fa fa-user">30</i>
                            </div>
                            <div class="review-wrap">
                                <ul class="rating">
                                    <li class="fa fa-star"></li>
                                    <li class="fa fa-star"></li>
                                    <li class="fa fa-star"></li>
                                    <li class="fa fa-star"></li>
                                    <li class="fa fa-star-half-empty"></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="cource-item blue-color">
                        <div class="cource-img">
                            <img src="images/courses/4.jpg" alt="">
                            <a class="image-link" href="courses-details.html" title="University Tour 2018">
                                <i class="fa fa-link"></i>
                            </a>
                            <span class="course-value">$450</span>
                            <span class="img-part"><img src="images/team/2.jpg" alt=""></span>
                        </div>
                        <div class="course-body">
                            <span><a href="#">BDS</a></span>
                            <h4 class="title"><a href="courses-details.html">Visual Studio Online Course</a></h4>

                        </div>
                        <div class="course-footer">
                            <div class="courses-seats">
                                <i class="fa fa-user">80</i>
                            </div>
                            <div class="review-wrap">
                                <ul class="rating">
                                    <li class="fa fa-star"></li>
                                    <li class="fa fa-star"></li>
                                    <li class="fa fa-star"></li>
                                    <li class="fa fa-star"></li>
                                    <li class="fa fa-star-half-empty"></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="cource-item blue-color">
                        <div class="cource-img">
                            <img src="images/courses/5.jpg" alt="">
                            <a class="image-link" href="courses-details.html" title="University Tour 2018">
                                <i class="fa fa-link"></i>
                            </a>
                            <span class="course-value">$450</span>
                            <span class="img-part"><img src="images/team/3.jpg" alt=""></span>
                        </div>
                        <div class="course-body">
                            <span><a href="#">Programming</a></span>
                            <h4 class="title"><a href="courses-details.html">Java Online Course</a></h4>

                        </div>
                        <div class="course-footer">
                            <div class="courses-seats">
                                <i class="fa fa-user">30</i>
                            </div>
                            <div class="review-wrap">
                                <ul class="rating">
                                    <li class="fa fa-star"></li>
                                    <li class="fa fa-star"></li>
                                    <li class="fa fa-star"></li>
                                    <li class="fa fa-star"></li>
                                    <li class="fa fa-star-half-empty"></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="cource-item blue-color">
                        <div class="cource-img">
                            <img src="images/courses/4.jpg" alt="">
                            <a class="image-link" href="courses-details.html" title="University Tour 2018">
                                <i class="fa fa-link"></i>
                            </a>
                            <span class="course-value">$450</span>
                            <span class="img-part"><img src="images/team/4.jpg" alt=""></span>
                        </div>
                        <div class="course-body">
                            <span><a href="#">Web Development</a></span>
                            <h4 class="title"><a href="courses-details.html">XML Online Course</a></h4>

                        </div>
                        <div class="course-footer">
                            <div class="courses-seats">
                                <i class="fa fa-user">30</i>
                            </div>
                            <div class="review-wrap">
                                <ul class="rating">
                                    <li class="fa fa-star"></li>
                                    <li class="fa fa-star"></li>
                                    <li class="fa fa-star"></li>
                                    <li class="fa fa-star"></li>
                                    <li class="fa fa-star-half-empty"></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="cource-item blue-color">
                        <div class="cource-img">
                            <img src="images/courses/5.jpg" alt="">
                            <a class="image-link" href="courses-details.html" title="University Tour 2018">
                                <i class="fa fa-link"></i>
                            </a>
                            <span class="course-value">$50</span>
                            <span class="img-part"><img src="images/team/5.jpg" alt=""></span>
                        </div>
                        <div class="course-body">
                            <span><a href="#">Science</a></span>
                            <h4 class="title"><a href="courses-details.html">Electrical Engineering</a></h4>

                        </div>
                        <div class="course-footer">
                            <div class="courses-seats">
                                <i class="fa fa-user">60</i>
                            </div>
                            <div class="review-wrap">
                                <ul class="rating">
                                    <li class="fa fa-star"></li>
                                    <li class="fa fa-star"></li>
                                    <li class="fa fa-star"></li>
                                    <li class="fa fa-star"></li>
                                    <li class="fa fa-star-half-empty"></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="cource-item blue-color">
                        <div class="cource-img">
                            <img src="images/courses/6.jpg" alt="">
                            <a class="image-link" href="courses-details.html" title="University Tour 2018">
                                <i class="fa fa-link"></i>
                            </a>
                            <span class="course-value">$20.00</span>
                            <span class="img-part"><img src="images/team/6.jpg" alt=""></span>
                        </div>
                        <div class="course-body">
                            <span><a href="#">Science</a></span>
                            <h4 class="title"><a href="courses-details.html">Electrical Engineering</a></h4>

                        </div>
                        <div class="course-footer">
                            <div class="courses-seats">
                                <i class="fa fa-user">30</i>
                            </div>
                            <div class="review-wrap">
                                <ul class="rating">
                                    <li class="fa fa-star"></li>
                                    <li class="fa fa-star"></li>
                                    <li class="fa fa-star"></li>
                                    <li class="fa fa-star"></li>
                                    <li class="fa fa-star-half-empty"></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 text-center">
                <div class="btn-part mt-20">
                    <a class="readon2" href="#">View All Courses</a>
                </div>
            </div>
        </div>
    </div>
    <!-- Courses End -->

    <!-- About Start -->
    <div class="rs-about-style7 bg13 d-none">
        <div class="container">
            <div class="content-part text-center">
                <div class="play-btn">
                    <a class="pulse-btn popup-youtube" href="https://www.youtube.com/watch?v=4R6aMH2FahQ"><i
                                class="fa fa-play"></i></a>
                </div>
                <span class="sub-title pb-10">We Are Best</span>
                <h2 class="title pb-15">5500+ STUDENTS TRUSTED ONLINE COURSES</h2>
                <p class="pb-30">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed eiusmod tempor incididunt ut laboreet
                    dolore magna<br> aliqua. Ut enim ad minim veniam, quised nostrud exercitation. Duis aute irure dolor
                    in reprehen derit in voluptate velit<br>
                    esse cillum dolore eu fugiat nulla pariatur
                </p>
            </div>
            <div class="col-lg-12 text-center">
                <a class="readon2" href="#">Get Started Now</a>
            </div>
        </div>
    </div>
    <!-- About End -->

    <!-- Team Start -->
    <div id="rs-team-style7" class="rs-team-style7 sec-spacer d-none">
        <div class="blue-overlay"></div>
        <div class="container">
            <div class="sec-title2 mb-50 text-center">
                <span class="title">Our Teachers</span>
                <h2 class="title"> Meet Our Expert Teacher</h2>
            </div>
            <div class="rs-carousel owl-carousel" data-loop="true" data-items="3" data-margin="30" data-autoplay="false"
                 data-autoplay-timeout="5000" data-smart-speed="1200" data-dots="true" data-nav="true"
                 data-nav-speed="false"
                 data-mobile-device="1" data-mobile-device-nav="true" data-mobile-device-dots="true"
                 data-ipad-device="2"
                 data-ipad-device-nav="true" data-ipad-device-dots="true" data-md-device="3" data-md-device-nav="true"
                 data-md-device-dots="true">
                <div class="item">
                    <div class="item-team">
                        <div class="team-img">
                            <img src="images/team/1.jpg" alt="">
                            <div class="social-icon">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                            </div>
                        </div>
                        <div class="team-content">
                            <h3 class="team-name"><a href="#">ABD Rashid Khan</a></h3>
                            <span class="sub-title">Vice Chancellor</span>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="item-team">
                        <div class="team-img">
                            <img src="images/team/2.jpg" alt="">
                            <div class="social-icon">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                            </div>
                        </div>
                        <div class="team-content">
                            <h3 class="team-name"><a href="#">Mahabub Alam</a></h3>
                            <span class="sub-title">Professor</span>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="item-team">
                        <div class="team-img">
                            <img src="images/team/3.jpg" alt="">
                            <div class="social-icon">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                            </div>
                        </div>
                        <div class="team-content">
                            <h3 class="team-name"><a href="#">John Doe</a></h3>
                            <span class="sub-title">Web Developer</span>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="item-team">
                        <div class="team-img">
                            <img src="images/team/4.jpg" alt="">
                            <div class="social-icon">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                            </div>
                        </div>
                        <div class="team-content">
                            <h3 class="team-name"><a href="#">Aden Hezard</a></h3>
                            <span class="sub-title">Ast. Professior</span>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="item-team">
                        <div class="team-img">
                            <img src="images/team/5.jpg" alt="">
                            <div class="social-icon">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                            </div>
                        </div>
                        <div class="team-content">
                            <h3 class="team-name"><a href="#">Jordan Lukako</a></h3>
                            <span class="sub-title">Professior</span>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="item-team">
                        <div class="team-img">
                            <img src="images/team/6.jpg" alt="">
                            <div class="social-icon">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                            </div>
                        </div>
                        <div class="team-content">
                            <h3 class="team-name"><a href="#">Luis Figo</a></h3>
                            <span class="sub-title">Instructor</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Team End -->

    <!-- Events Start -->
    <div class="rs-countdown-part bg16 d-none">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-7">
                    <div class="countdown-part">
                        <div class="content-part">
                            <span class="sub-title"> Register Now</span>
                            <h2 class="title">Get 50+ Courses<br>
                                Free Access</h2>
                            <p class="description">
                                We create Premium WordPress themes & plugins for more than three years.<br>
                                Our team goal is to reunite the elegance of unique.
                            </p>
                        </div>
                        <div class="counter-wrap">
                            <div class="timecounter-inner">
                                <div class="CountDownTimer" data-date="2020-12-20 23:59:59"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="register-form">
                        <div class="form-title text-center">
                            <h4 class="title">Register now for getting 50+<br>
                                courses free access. This offer<br>
                                for limited time.</h4>
                        </div>
                        <div class="form-group text-center">
                            <form id="contact-form" class="contact-form" method="post" action="mailer.php">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <input class="from-control" name="name" type="text" placeholder="Name" id="name"
                                               required="required">

                                    </div>
                                    <div class="col-lg-12">
                                        <input class="from-control" name="email" type="email" placeholder="E-Mail"
                                               id="email" required="required">
                                    </div>
                                    <div class="col-lg-12">
                                        <input class="from-control" name="number" type="text" placeholder="Phone Number"
                                               id="phone_number" required="required">
                                    </div>
                                    <div class="col-lg-12">
                                        <input class="from-control" name="subject" type="text" placeholder="Subject"
                                               id="subject" required="required">
                                    </div>
                                    <div class="col-lg-12">
                                        <input type="submit" value="Get It Now" class="wpcf7-form-control wpcf7-submit">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Events End -->

    <!-- Latest News Start -->
    <div id="rs-latest-news-style7" class="rs-latest-news-style7 sec-spacer d-none">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h2 class="title pb-50 md-pb-30 uppercase">Our latest New</h2>
                    <div class="rs-latest-list">
                        <div class="latest-wrap">
                            <div class="news-list-block">
                                <div class="news-list-item mb-40">
                                    <div class="news-img">
                                        <img src="images/blog/latest/1.jpg" alt="">
                                    </div>
                                    <div class="news-content">
                                        <div class="news-date">
                                            <span>June 21, 2020</span>
                                        </div>
                                        <h4 class="news-title">
                                            <a href="#">While the lovely valley team work</a>
                                        </h4>
                                        <div class="news-btn">
                                            <a class="readon2 transparent" href="#"> Read More</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="news-list-item mb-40">
                                    <div class="news-img">
                                        <img src="images/blog/latest/2.jpg" alt="">
                                    </div>
                                    <div class="news-content">
                                        <div class="news-date">
                                            <span>June 21, 2020</span>
                                        </div>
                                        <h4 class="news-title">
                                            <a href="#">I should be incapable of drawing</a>
                                        </h4>
                                        <div class="news-btn">
                                            <a class="readon2 transparent" href="#"> Read More</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="news-list-item">
                                    <div class="news-img">
                                        <img src="images/blog/latest/3.jpg" alt="">
                                    </div>
                                    <div class="news-content">
                                        <div class="news-date">
                                            <span>June 21, 2020</span>
                                        </div>
                                        <h4 class="news-title">
                                            <a href="#">I must explain to you how all this idea</a>
                                        </h4>
                                        <div class="news-btn">
                                            <a class="readon2 transparent" href="#"> Read More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 mb-pt-50">
                    <h2 class="title pb-50 md-pb-30 uppercase">Our latest Events</h2>
                    <div class="rs-latest-list">
                        <div class="event-item-new mb-50">
                            <div class="event-date">
                                <div class="vertical-align">
                                    <span class="day">05</span>
                                    <span class="month">Sep</span>
                                </div>
                            </div>
                            <div class="event-des">
                                <h4 class="title">
                                    <a href="#">Eshkool Gala Day For Kids</a>
                                </h4>
                                <p>Event Description. A party is a gathering of people who have been invited by a host
                                    for the purposes of… </p>

                            </div>
                        </div>
                        <div class="event-item-new mb-50">
                            <div class="event-date">
                                <div class="vertical-align">
                                    <span class="day">10</span>
                                    <span class="month">Aug</span>
                                </div>
                            </div>
                            <div class="event-des">
                                <h4 class="title">
                                    <a href="#">Kids Study Tour 2020.</a>
                                </h4>
                                <p>Event Description. A party is a gathering of people who have been invited by a host
                                    for the purposes of… </p>

                            </div>
                        </div>
                        <div class="event-item-new mb-10">
                            <div class="event-date">
                                <div class="vertical-align">
                                    <span class="day">05</span>
                                    <span class="month">Aug</span>
                                </div>
                            </div>
                            <div class="event-des">
                                <h4 class="title">
                                    <a href="#">Kids Drawing Content</a>
                                </h4>
                                <p>Event Description. A party is a gathering of people who have been invited by a host
                                    for the purposes of… </p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Partner Start -->
    <div id="rs-partner" class="rs-partner  sec-padding-bottom-150">
        <div class="container d-none">
            <div class="sec-title2 mb-50 text-center">
                <span class="title">Certificate</span>
                <h2 class="title"> Certificate We Give</h2>
            </div>
            <div class="rs-carousel owl-carousel" data-loop="true" data-items="5" data-margin="80" data-autoplay="true"
                 data-autoplay-timeout="6000" data-smart-speed="2000" data-dots="false" data-nav="false"
                 data-nav-speed="false" data-mobile-device="2" data-mobile-device-nav="false"
                 data-mobile-device-dots="false"
                 data-ipad-device="4" data-ipad-device-nav="false" data-ipad-device-dots="false" data-md-device="5"
                 data-md-device-nav="false" data-md-device-dots="false">
                <div class="partner-item">
                    <a href="#">
                        <div class="carousel-01">
                            <img src="" alt="">
                            <h4>Content 01</h4>
                        </div>
                    </a>
                    <div class="owl-carousel owl-theme">
                        <div class="item">
                            <h4>1</h4>
                        </div>
                        <div class="item">
                            <h4>2</h4>
                        </div>
                        <div class="item">
                            <h4>3</h4>
                        </div>
                        <div class="item">
                            <h4>4</h4>
                        </div>
                        <div class="item">
                            <h4>5</h4>
                        </div>
                        <div class="item">
                            <h4>6</h4>
                        </div>
                        <div class="item">
                            <h4>7</h4>
                        </div>
                        <div class="item">
                            <h4>8</h4>
                        </div>
                        <div class="item">
                            <h4>9</h4>
                        </div>
                        <div class="item">
                            <h4>10</h4>
                        </div>
                        <div class="item">
                            <h4>11</h4>
                        </div>
                        <div class="item">
                            <h4>12</h4>
                        </div>
                    </div>
                </div>
                <!-- <div class="partner-item">
                        <a href="#"><img src="images/partner/2.png" alt="Partner Image"></a>
                    </div>
                    <div class="partner-item">
                        <a href="#"><img src="images/partner/3.png" alt="Partner Image"></a>
                    </div>
                    <div class="partner-item">
                        <a href="#"><img src="images/partner/4.png" alt="Partner Image"></a>
                    </div>
                    <div class="partner-item">
                        <a href="#"><img src="images/partner/5.png" alt="Partner Image"></a>
                    </div> -->
            </div>
        </div>
        <div id="rs-team-style7" class="rs-team-style7 sec-padding-50">
            <div class="blue-overlay"></div>
            <div class="container">
                <div class="sec-title2 mb-50 text-center">
                    <span class="title"></span>
                    <h2 class="title"></h2>
                </div>
                <div class="rs-carousel owl-carousel" data-loop="true" data-items="3" data-margin="30"
                     data-autoplay="false"
                     data-autoplay-timeout="5000" data-smart-speed="1200" data-dots="true" data-nav="true"
                     data-nav-speed="false" data-mobile-device="1" data-mobile-device-nav="true"
                     data-mobile-device-dots="true" data-ipad-device="2" data-ipad-device-nav="true"
                     data-ipad-device-dots="true" data-md-device="3" data-md-device-nav="true"
                     data-md-device-dots="true">
                    <div class="item">
                        <div class="item-team">
                            <div class="team-img">
                                <img src="images/home2-slider/2.jpg" alt="">
                                <!-- <div class="social-icon">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                            </div> -->
                            </div>
                            <div class="team-content">
                                <h3 class="team-name"><a href="#">Content-2</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="item-team">
                            <div class="team-img">
                                <img src="images/home2-slider/3.jpg" alt="">
                                <!-- <div class="social-icon">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                            </div> -->
                            </div>
                            <div class="team-content">
                                <h3 class="team-name"><a href="#">Content-3</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="item-team">
                            <div class="team-img">
                                <img src="images/home2-slider/4.jpg" alt="">
                                <!-- <div class="social-icon">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                            </div> -->
                            </div>
                            <div class="team-content">
                                <h3 class="team-name"><a href="#">Content-4</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="item-team">
                            <div class="team-img">
                                <img src="images/home2-slider/5.jpg" alt="">
                                <!-- <div class="social-icon">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                            </div> -->
                            </div>
                            <div class="team-content">
                                <h3 class="team-name"><a href="#">Content-5</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="item-team">
                            <div class="team-img">
                                <img src="images/home2-slider/1.jpg" alt="">
                                <!-- <div class="social-icon">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                            </div> -->
                            </div>
                            <div class="team-content">
                                <h3 class="team-name"><a href="#">Content-6</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="item-team">
                            <div class="team-img">
                                <img src="images/home2-slider/2.jpg" alt="">
                                <!-- <div class="social-icon">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                            </div> -->
                            </div>
                            <div class="team-content">
                                <h3 class="team-name"><a href="#">Content-7</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="item-team">
                            <div class="team-img">
                                <img src="images/home2-slider/3.jpg" alt="">
                                <!-- <div class="social-icon">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                            </div> -->
                            </div>
                            <div class="team-content">
                                <h3 class="team-name"><a href="#">Content-8</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="item-team">
                            <div class="team-img">
                                <img src="images/home2-slider/4.jpg" alt="">
                                <!-- <div class="social-icon">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                            </div> -->
                            </div>
                            <div class="team-content">
                                <h3 class="team-name"><a href="#">Content-9</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="item-team">
                            <div class="team-img">
                                <img src="images/home2-slider/5.jpg" alt="">
                                <!-- <div class="social-icon">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                            </div> -->
                            </div>
                            <div class="team-content">
                                <h3 class="team-name"><a href="#">Content-10</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="item-team">
                            <div class="team-img">
                                <img src="images/home2-slider/2.jpg" alt="">
                                <!-- <div class="social-icon">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                            </div> -->
                            </div>
                            <div class="team-content">
                                <h3 class="team-name"><a href="#">Content-1</a></h3>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <section class="paragraph paragraph--type--promos paragraph--view-mode--default layout--carousel d-none">
        <div class="container container--centered container--promos">
            <div class="promo-margin-container slick-initialized slick-slider">
                <button
                        class="slick-prev slick-arrow slick-disabled" aria-label="Previous" type="button"
                        aria-disabled="true"
                >Previous
                </button>


                <div class="slick-list draggable">
                    <div class="slick-track" style="opacity: 1; width: 2475px; transform: translate3d(0px, 0px, 0px);">
                        <article
                                class="promo-image promo type--regular with-link slick-slide slick-current slick-active"
                                data-slick-index="0" aria-hidden="false" style="width: 275px;" tabindex="0">


                            <a class="fullsize with-image" href="#" tabindex="0">
                                <span>carousel-1</span>

                            </a>
                            <div class="inside with-image">
                                <div class="item__media">
                                    <div class="flex-fix focalarea--mc"
                                         style="background-image: url('images/home2-slider/2.jpg');">
                                        <h2 class="linkhover"><span>carousel-1</span>
                                        </h2>
                                    </div>
                                </div>
                                <div class="item__content">
                                    <h2 class="has-link"><span>carousel-1</span>
                                    </h2>
                                    <div class="content-area"></div>
                                </div>
                            </div>
                        </article>
                        <article class="promo-image promo type--regular with-link slick-slide slick-active"
                                 data-slick-index="1" aria-hidden="false" style="width: 275px;" tabindex="0">


                            <a class="fullsize with-image" href="#"
                               tabindex="0">
                                <span>carousel-2</span>

                            </a>
                            <div class="inside with-image">
                                <div class="item__media">
                                    <div class="flex-fix focalarea--mc"
                                         style="background-image: url('images/home2-slider/2.jpg');">
                                        <h2 class="linkhover"><span>carousel-2</span>
                                        </h2>
                                    </div>
                                </div>
                                <div class="item__content">
                                    <h2 class="has-link"><span>carousel-2</span>
                                    </h2>
                                    <div class="content-area"></div>
                                </div>
                            </div>
                        </article>
                        <article class="promo-image promo type--regular with-link slick-slide slick-active"
                                 data-slick-index="2" aria-hidden="false" style="width: 275px;" tabindex="0">


                            <a class="fullsize with-image" href="#"
                               tabindex="0">
                                <span>carousel-3</span>

                            </a>
                            <div class="inside with-image">
                                <div class="item__media">
                                    <div class="flex-fix focalarea--mc"
                                         style="background-image: url('images/home2-slider/2.jpg');">
                                        <h2 class="linkhover"><span>carousel-3</span>
                                        </h2>
                                    </div>
                                </div>
                                <div class="item__content">
                                    <h2 class="has-link"><span>carousel-3</span>
                                    </h2>
                                    <div class="content-area"></div>
                                </div>
                            </div>
                        </article>
                        <article class="promo-image promo type--regular with-link slick-slide slick-active"
                                 data-slick-index="3" aria-hidden="false" style="width: 275px;" tabindex="0">


                            <a class="fullsize with-image"
                               href="#" tabindex="0">
                                <span>carousel-4</span>

                            </a>
                            <div class="inside with-image">
                                <div class="item__media">
                                    <div class="flex-fix focalarea--mc"
                                         style="background-image: url('images/home2-slider/2.jpg');">
                                        <h2 class="linkhover"><span>carousel-4</span>
                                        </h2>
                                    </div>
                                </div>
                                <div class="item__content">
                                    <h2 class="has-link"><span>carousel-4</span>
                                    </h2>
                                    <div class="content-area"></div>
                                </div>
                            </div>
                        </article>
                        <article class="promo-image promo type--regular with-link slick-slide" data-slick-index="4"
                                 aria-hidden="true" style="width: 275px;" tabindex="-1">


                            <a class="fullsize with-image" href="#" tabindex="-1">
                                <span>carousel-5</span>

                            </a>
                            <div class="inside with-image">
                                <div class="item__media">
                                    <div class="flex-fix focalarea--tc"
                                         style="background-image: url('images/home2-slider/2.jpg');">
                                        <h2 class="linkhover"><span>carousel-5</span>
                                        </h2>
                                    </div>
                                </div>
                                <div class="item__content">
                                    <h2 class="has-link"><span>carousel-5</span>
                                    </h2>
                                    <div class="content-area"></div>
                                </div>
                            </div>
                        </article>
                        <article class="promo-image promo type--regular with-link slick-slide" data-slick-index="5"
                                 aria-hidden="true" style="width: 275px;" tabindex="-1">


                            <a class="fullsize with-image" href="#" tabindex="-1">
                                <span>carousel-6</span>

                            </a>
                            <div class="inside with-image">
                                <div class="item__media">
                                    <div class="flex-fix focalarea--mc"
                                         style="background-image: url('images/home2-slider/2.jpg');">
                                        <h2 class="linkhover"><span>carousel-6</span>
                                        </h2>
                                    </div>
                                </div>
                                <div class="item__content">
                                    <h2 class="has-link"><span>carousel-6</span>
                                    </h2>
                                    <div class="content-area"></div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>


                <button class="slick-next slick-arrow" aria-label="Next" type="button" style=""
                        aria-disabled="false">Next
                </button>
            </div>
        </div>
    </section>
    <!-- Partner End -->

    <!-- Latest News End -->

<?php include('footer.php'); ?>
<?php include('header.php'); ?>
<div class="not-home-page">
<!-- NEWS -->
<section class="paragraph paragraph--type--promos paragraph--view-mode--default layout--quintuple">
    <div class="container container--centered container--promos">
        <div class="list-header">
            <h2 class="list-header__header">Courses</h2>
            <span class="list-header__label">All Courses of CMU</span>
            <a href="#" target="_blank" class="list-header__icon list-header__icon--rss list-header__item--mla">
                <span></span>
            </a>
        </div>
        <div class=""> 
            <div class="row">
                <div class="col-md-4 col-sm-4 col-6">
                    <a href="single-course.php" class="content-img-title-short-des">
                        <div class="row">
                            <div class="col-4 d-flex align-items-center">
                                <img src="images/home2-slider/2.jpg" alt="">
                            </div>
                            <div class="col-8">
                                <h4>MBBS</h4>
                                <p>This is the short description of MBBS</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 col-sm-4 col-6">
                    <a href="single-course.php" class="content-img-title-short-des">
                        <div class="row">
                            <div class="col-4 d-flex align-items-center">
                                <img src="images/home2-slider/2.jpg" alt="">
                            </div>
                            <div class="col-8">
                                <h4>BDS</h4>
                                <p>This is the short description of BDS</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 col-sm-4 col-6">
                    <a href="single-course.php" class="content-img-title-short-des">
                        <div class="row">
                            <div class="col-4 d-flex align-items-center">
                                <img src="images/home2-slider/2.jpg" alt="">
                            </div>
                            <div class="col-8">
                                <h4>Nursing</h4>
                                <p>This is the short description of Nursing</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 col-sm-4 col-6">
                    <a href="single-course.php" class="content-img-title-short-des">
                        <div class="row">
                            <div class="col-4 d-flex align-items-center">
                                <img src="images/home2-slider/2.jpg" alt="">
                            </div>
                            <div class="col-8">
                                <h4>Unani</h4>
                                <p>This is the short description of Unani</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 col-sm-4 col-6">
                    <a href="single-course.php" class="content-img-title-short-des">
                        <div class="row">
                            <div class="col-4 d-flex align-items-center">
                                <img src="images/home2-slider/2.jpg" alt="">
                            </div>
                            <div class="col-8">
                                <h4>IMT</h4>
                                <p>This is the short description of IMT</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 col-sm-4 col-6">
                    <a href="single-course.php" class="content-img-title-short-des">
                        <div class="row">
                            <div class="col-4 d-flex align-items-center">
                                <img src="images/home2-slider/2.jpg" alt="">
                            </div>
                            <div class="col-8">
                                <h4>Optometry</h4>
                                <p>This is the short description of Optometry</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- NEWS -->

</div>
<?php include('footer.php'); ?>

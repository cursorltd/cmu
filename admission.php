<?php include('header.php'); ?>
<div class="not-home-page university-home container">
    <div class="vc-msg-area">
        <div class="list-header">
            <h2 class="list-header__header">Admission</h2>
            <a href="#" target="_blank" class="list-header__icon list-header__icon--rss list-header__item--mla">
                <span></span>
            </a>
        </div>
        <div>
        </div>
        <div class="">
            <h4>Admission Notice</h4>
            <p style="text-align: justify;">
                It is a long established fact that a reader will be distracted by the readable content of a page when
                looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution
                of letters, as opposed to using 'Content here, content here', making it look like readable English.
            </p>
            <h4>Admission Result</h4>
            <p style="text-align: justify;">
                It is a long established fact that a reader will be distracted by the readable content of a page when
                looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution
                of letters, as opposed to using 'Content here, content here', making it look like readable English.
            </p>
            <h4>Waiting List</h4>
            <p style="text-align: justify;">
                It is a long established fact that a reader will be distracted by the readable content of a page when
                looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution
                of letters, as opposed to using 'Content here, content here', making it look like readable English.
            </p>
            <h4>
                Admission Procedures
            </h4>
            <p style="text-align: justify;">
                It is a long established fact that a reader will be distracted by the readable content of a page when
                looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution
                of letters, as opposed to using 'Content here, content here', making it look like readable English.
            </p>
            <h4>
                Payment Procedure
            </h4>
            <p style="text-align: justify;">
                It is a long established fact that a reader will be distracted by the readable content of a page when
                looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution
                of letters, as opposed to using 'Content here, content here', making it look like readable English.
            </p>

        </div>

    </div>

</div>
<?php include('footer.php'); ?>

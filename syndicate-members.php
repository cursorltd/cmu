<?php include('header.php'); ?>
<div class="not-home-page university-home container">
    <div class="vc-msg-area">
        <div class="list-header">
            <h2 class="list-header__header">Syndicate Members</h2>
             <a href="#" target="_blank" class="list-header__icon list-header__icon--rss list-header__item--mla">
                <span></span>
            </a>
        </div>
        <div >
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-4 col-6">
                    <div class="user-short-profile usp-vertical-img">
                        <a href="" class="content-img-title-short-des">
                            <div class="row ">
                                <div class="col-12 d-flex align-items-center">
                                    <img src="images/team/1.jpg" alt="">
                                </div>
                                <div class="col-12">
                                    <h5>অধ্যাপক ডা. মো. ইসমাইল খান</h5>
                                    <p class="text-center">ভাইস চ্যান্সেলর, চট্টগ্রাম মেডিকেল বিশ্ববিদ্যালয়</p>
<!--                                    <p>This is the short description of Staff Name 01</p>-->
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-4 col-6">
                    <div class="user-short-profile usp-vertical-img">
                        <a href="" class="content-img-title-short-des">
                            <div class="row ">
                                <div class="col-12 d-flex align-items-center">
                                    <img src="images/team/3.jpg" alt="">
                                </div>
                                <div class="col-12">
                                    <h5>ডা. মো. আফছারুল আমীন</h5>
                                    <p class="text-center">মাননীয় সংসদ সদস্য</p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-4 col-6">
                    <div class="user-short-profile usp-vertical-img">
                        <a href="" class="content-img-title-short-des">
                            <div class="row ">
                                <div class="col-12 d-flex align-items-center">
                                    <img src="images/team/2.jpg" alt="">
                                </div>
                                <div class="col-12">
                                    <h5>জনাব বেগম সেলিমা আহমাদ</h5>
                                    <p class="text-center">মাননীয় সংসদ সদস্য</p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-4 col-6">
                    <div class="user-short-profile usp-vertical-img">
                        <a href="" class="content-img-title-short-des">
                            <div class="row ">
                                <div class="col-12 d-flex align-items-center">
                                    <img src="images/team/3.jpg" alt="">
                                </div>
                                <div class="col-12">
                                    <h5>অধ্যাপক ডা. আবু নাসার রিজভী</h5>
                                    <p class="text-center">পরিচালক ( পরিকল্পনা ও উন্নয়ন ) ও অধ্যাপক, নিউরোলজী বিভাগ, বঙ্গবন্ধু শেখ মুজিব মেডিকেল বিশ্ববিদ্যালয়</p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
<?php include('footer.php'); ?>

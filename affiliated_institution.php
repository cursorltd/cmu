<?php include('header.php'); ?>
<div class="not-home-page university-home container">
    <div class="">
        <div class="list-header">
            <h2 class="list-header__header">Affiliated Institution</h2>
            <a href="#" target="_blank" class="list-header__icon list-header__icon--rss list-header__item--mla">
                <span></span>
            </a>
        </div>

        <div class="row pt-5">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                <ul class="nav nav-tabs mb-5" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active " id="home-tab" data-toggle="tab" href="#home" role="tab"
                           aria-controls="home" aria-selected="true">Government</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="staff-tab" data-toggle="tab" href="#staff" role="tab"
                           aria-controls="staff" aria-selected="false">Non-Government</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <h4>সরকারী মেডিকেল কলেজসমূহ</h4>
                        <table class="table table-striped">
                            <thead>
                            <tr><th class="text-center">SL</th><th class="text-center">কলেজের নাম</th><th class="text-center">কলেজ কোড</th></tr>
                            </thead>
                            <tbody><tr>
                                <td class="text-center">01</td>
                                <td><a href="institute-details/30">চট্টগ্রাম মেডিকেল কলেজ</a></td>
                                <td class="text-center">০১</td>
                            </tr>
                            <tr>
                                <td class="text-center">02</td>
                                <td><a href="institute-details/31">কুমিল্লা মেডিকেল কলেজ</a></td>
                                <td class="text-center">০২</td>
                            </tr>
                            <tr>
                                <td class="text-center">03</td>
                                <td><a href="institute-details/32">আব্দুল মালেক উকিল মেডিকেল কলেজ</a></td>
                                <td class="text-center">০৩</td>
                            </tr>
                            <tr>
                                <td class="text-center">04</td>
                                <td><a href="institute-details/33">কক্সবাজার মেডিকেল কলেজ</a></td>
                                <td class="text-center">০৪</td>
                            </tr>
                            <tr>
                                <td class="text-center">05</td>
                                <td><a href="institute-details/34">রাঙ্গামাটি মেডিকেল কলেজ</a></td>
                                <td class="text-center">০৫</td>
                            </tr>
                            <tr>
                                <td class="text-center">06</td>
                                <td><a href="institute-details/35">চাঁদপুর মেডিকেল কলেজ</a></td>
                                <td class="text-center">২৭</td>
                            </tr>
                            </tbody></table>
                    </div>
                    <div class="tab-pane fade" id="staff" role="tabpanel" aria-labelledby="staff-tab">
                        <h4>বেসরকারী মেডিকেল কলেজসমূহ</h4>
                        <table class="table table-striped">
                            <thead>
                            <tr><th class="text-center">SL.</th><th class="text-center">কলেজের নাম</th><th class="text-center">কলেজ কোড</th></tr>
                            </thead>
                            <tbody><tr>
                                <td class="text-center">01</td>
                                <td><a href="institute-details/36">চট্টগ্রাম মা ও শিশু হাসপাতাল মেডিকেল কলেজ</a></td>
                                <td class="text-center">০৬</td>
                            </tr>
                            <tr>
                                <td class="text-center">02</td>
                                <td><a href="institute-details/37">বিজিসি ট্রাস্ট মেডিকেল কলেজ</a></td>
                                <td class="text-center">০৭</td>
                            </tr>
                            <tr>
                                <td class="text-center">03</td>
                                <td><a href="institute-details/38">সাউদার্ন মেডিকেল কলেজ</a></td>
                                <td class="text-center">০৮</td>
                            </tr>
                            <tr>
                                <td class="text-center">04</td>
                                <td><a href="institute-details/41">মেরিন সিটি মেডিকেল কলেজ</a></td>
                                <td class="text-center">০৯</td>
                            </tr>
                            <tr>
                                <td class="text-center">05</td>
                                <td><a href="institute-details/42">চট্টগ্রাম ইন্টারন্যাশনাল মেডিকেল কলেজ</a></td>
                                <td class="text-center">১০</td>
                            </tr>
                            <tr>
                                <td class="text-center">06</td>
                                <td><a href="institute-details/43">ইস্টার্ন মেডিকেল কলেজ</a></td>
                                <td class="text-center">১১</td>
                            </tr>
                            <tr>
                                <td class="text-center">07</td>
                                <td><a href="institute-details/44">সেন্ট্রাল মেডিকেল কলেজ</a></td>
                                <td class="text-center">১২</td>
                            </tr>
                            <tr>
                                <td class="text-center">08</td>
                                <td><a href="institute-details/45">ময়নামতি মেডিকেল কলেজ</a></td>
                                <td class="text-center">১৩</td>
                            </tr>
                            <tr>
                                <td class="text-center">09</td>
                                <td><a href="institute-details/46">ব্রাহ্মণবাড়ীয়া মেডিকেল কলেজ</a></td>
                                <td class="text-center">১৪</td>
                            </tr>
                            <tr>
                                <td class="text-center">10</td>
                                <td><a href="institute-details/47">ইনস্টিটিউট অব অ্যাপ্লাইড হেলথ সায়েন্সেস</a></td>
                                <td class="text-center">১৫</td>
                            </tr>
                            </tbody></table>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
<?php include('footer.php'); ?>

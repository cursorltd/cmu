<!DOCTYPE html>
<html lang="zxx">
<head>
    <!-- meta tag -->
    <meta charset="utf-8">
    <title>Chittagong Medical University</title>
    <meta name="description" content="">
    <!-- responsive tag -->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="shortcut icon" type="image/x-icon" href="images/fav.png">
    <!-- bootstrap v4 css -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <!-- font-awesome css -->
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <!-- animate css -->
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <!-- owl.carousel css -->
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
    <!-- slick css -->
    <link rel="stylesheet" type="text/css" href="css/slick.css">
    <!-- magnific popup css -->
    <link rel="stylesheet" type="text/css" href="css/magnific-popup.css">
    <!-- Offcanvas CSS -->
    <link rel="stylesheet" type="text/css" href="css/off-canvas.css">
    <!-- flaticon css  -->
    <link rel="stylesheet" type="text/css" href="fonts/flaticon.css">
    <!-- flaticon2 css  -->
    <link rel="stylesheet" type="text/css" href="fonts/fonts2/flaticon.css">
    <!-- rsmenu CSS -->
    <link rel="stylesheet" type="text/css" href="css/rsmenu-main.css">
    <!-- rsmenu transitions CSS -->
    <link rel="stylesheet" type="text/css" href="css/rsmenu-transitions.css">
    <!-- ki.se stylesheet -->
    <!-- <link rel="stylesheet" media="all" href="css/css_5jLwqPGdwtZOCppI12Yrz1441vSNjOOOXv7I4ynjHig.css" />
    <link rel="stylesheet" media="all" href="css/css_X8XY0qGGeRmUbcZ5p251ChB6WYhj-XyLtf1q_jbYXrE.css" /> -->
    <!-- style css -->
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="stylesheet" type="text/css" href="css/style2.css">
    <link rel="stylesheet" type="text/css" href="css/style3.css">
    <!-- Spacing css -->
    <link rel="stylesheet" type="text/css" href="css/spacing.css">
    <!-- responsive css -->
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="university-home">
<!--Preloader area start here-->
<div class="book_preload">
    <div class="book rs-animation-scale-up">
        <i class="fa fa-heartbeat" aria-hidden="true"></i>

        <!--        <div class="book__page"></div>-->
        <!--        <div class="book__page"></div>-->
        <!--        <div class="book__page"></div>-->
    </div>
</div>
<!--Preloader area end here-->

<!--Full width header Start-->
<div class="full-width-header">

    <!--Header Start-->
    <header id="rs-header" class="rs-header">
        <!-- Menu Start -->
        <!--        <div class="top-header-bg">-->
        <!--            <div class="container">-->
        <!--                <div class="row">-->
        <!--                    <div class="col-12 top-header">-->
        <!--                        <h2>Chittagong Medical University</h2>-->
        <!--                    </div>-->
        <!--                </div>-->
        <!--            </div>-->
        <!--        </div>-->
        <div class="menu-area menu-sticky">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-12">
                        <div class="menu-area-in">
                            <div class="logo-area">
                                <a href="index.php"><img src="images/Asset 2.png" alt="logo"></a>
                                <!--<h4>Chittagong Medical University</h4>-->
                            </div>
                            <div class="header-title-area">
                                <a href="index.php">
                                    <h3 class="text-white">চট্টগ্রাম মেডিকেল বিশ্ববিদ্যালয়</h3>
                                    <h4 class="text-white">Chittagong Medical University</h4>
                                </a>
                            </div>
                            <div style="clear: both;"></div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="main-menu">
                            <!-- <div id="logo-sticky" class="text-center">
                                            <a href="index.html"><img src="images/logo.png" alt="logo"></a>
                                        </div> -->
                            <a class="rs-menu-toggle" style="padding-left: 10px;"><i class="fa fa-bars"></i>Menu</a>
                            <nav class="rs-menu">
                                <ul class="nav-menu">
                                    <!-- Home -->
                                    <li class="current-menu-item current_page_item">
                                        <select name="" id=""
                                                style="color: #fff; margin-top: 15px; font-size: 16px; padding: 0 5px">
                                            <option value="English" style="color: black;">ENGLISH</option>
                                            <option value="Bangla" style="color: black;">BANGLA</option>
                                        </select>
                                        <span class="current-menu-item-bar">
                                                <i class="fa fa-globe" aria-hidden="true" style="color: white;"></i>
                                            </span>
                                    </li>
                                    <li class="current-menu-item current_page_item">
                                        <a href="#" id="menu" data-menu="1" onclick="menuOpen();" class="home">Menu
                                            <!--                                            <img style="width: 20px; margin-bottom: 5px;"-->
                                            <!--                                                 src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pg0KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE2LjAuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPg0KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCINCgkgd2lkdGg9IjEyNHB4IiBoZWlnaHQ9IjEyNHB4IiB2aWV3Qm94PSIwIDAgMTI0IDEyNCIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMTI0IDEyNDsiIHhtbDpzcGFjZT0icHJlc2VydmUiPg0KPGc+DQoJPHBhdGggZD0iTTExMiw2SDEyQzUuNCw2LDAsMTEuNCwwLDE4czUuNCwxMiwxMiwxMmgxMDBjNi42LDAsMTItNS40LDEyLTEyUzExOC42LDYsMTEyLDZ6Ii8+DQoJPHBhdGggZD0iTTExMiw1MEgxMkM1LjQsNTAsMCw1NS40LDAsNjJjMCw2LjYsNS40LDEyLDEyLDEyaDEwMGM2LjYsMCwxMi01LjQsMTItMTJDMTI0LDU1LjQsMTE4LjYsNTAsMTEyLDUweiIvPg0KCTxwYXRoIGQ9Ik0xMTIsOTRIMTJjLTYuNiwwLTEyLDUuNC0xMiwxMnM1LjQsMTIsMTIsMTJoMTAwYzYuNiwwLDEyLTUuNCwxMi0xMlMxMTguNiw5NCwxMTIsOTR6Ii8+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8L3N2Zz4NCg==" />-->
                                            <span class="current-menu-item-bar">
                                                <i class="fa fa-bars" aria-hidden="true"></i>
                                            </span>
                                            <span class="current-menu-item-bar" style="display: none;">
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                            </span>
                                        </a>
                                    </li>
                                    <!-- End Home -->

                                    <!--About Menu Start-->
                                    <!-- <li class="menu-item-has-children"> <a href="#">About CMU</a>
                                        <ul class="sub-menu">
                                            <li> <a href="#">Vice Chancellor's Message</a></li>
                                            <li><a href="#">Vision, Mission & Objective</a></li>
                                            <li><a href="#">Syndicate Members</a></li>
                                            <li><a href="#">CMU Act</a></li>
                                        </ul>
                                    </li> -->
                                    <!--About Menu End-->

                                    <!-- Drop Down -->
                                    <!-- <li class="menu-item-has-children"> <a href="#">Academic</a>
                                        <ul class="sub-menu">
                                            <li> <a href="#">Admission</a></li>
                                            <li> <a href="#">Academic Date</a></li>
                                        </ul>
                                    </li> -->
                                    <!--End Icons -->

                                    <!--Courses Menu Start-->
                                    <!-- <li class="menu-item-has-children"> <a href="#">Courses</a>
                                        <ul class="sub-menu">
                                            <li> <a href="#">MBBS</a> </li>
                                            <li> <a href="#">BDS</a></li>
                                            <li> <a href="#">Nursing</a> </li>
                                            <li> <a href="#">Unani</a> </li>
                                            <li> <a href="#">IMT</a> </li>
                                            <li> <a href="#">Optometry</a> </li>
                                        </ul>
                                    </li> -->
                                    <!--Courses Menu End-->

                                    <!--blog Menu Start-->
                                    <!-- <li class="menu-item-has-children"> <a href="#">Others</a>
                                        <ul class="sub-menu">
                                            <li class="menu-item-has-children"> <a href="#">Affiliated
                                                    Institution</a>
                                                <ul class="sub-menu">
                                                    <li><a href="">Government</a></li>
                                                    <li><a href="">Non-Government</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-has-children"> <a href="#">Administration</a>
                                                <ul class="sub-menu">
                                                    <li><a href="">Chancellor</a></li>
                                                    <li><a href="">Vice Chancellor</a></li>
                                                    <li><a href="">Pro Vice Chancellor</a></li>
                                                    <li><a href="">Treasurer</a></li>
                                                    <li><a href="">Administrative Department</a></li>
                                                </ul>
                                            </li>
                                            <li> <a href="#">Notice</a> </li>
                                            <li> <a href="#">Research</a> </li>
                                        </ul>
                                    </li> -->
                                    <!--blog Menu End-->

                                    <!--Events Menu Start-->
                                    <!-- <li> <a href="#">Results</a> </li> -->
                                    <!--Events Menu End-->

                                    <!--Contact Menu Start-->
                                    <!-- <li> <a href="#">Contact</a></li> -->
                                    <!--Contact Menu End-->
                                    <li>
                                        <div class="searce-box">
                                            <a class="rs-search" data-target=".search-modal" data-toggle="modal"
                                               href="#"><i class="fa fa-search"></i></a>
                                        </div>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Menu End -->
    </header>
    <!--Header End-->

</div>
<!--Full width header End-->
<div id="parentDiv" style="display: none;">
    <div class="container" style="margin-top: 60px">
        <div class="row menu">
            <div class="col-3 menu-item">
                <a href="admission.php">
                    <h4>Admission</h4>
                </a>
            </div>
            <div class="col-3 menu-item">
                <a href="academic-date.php">
                    <h4>Academic Date</h4>
                </a>
            </div>
            <div class="col-3 menu-item">
                <a href="affiliated_institution.php">
                    <h4>Affiliated Institution</h4>
                </a>
            </div>
            <div class="col-3 menu-item">
                <a href="">
                    <h4>Administration</h4>
                </a>
            </div>
            <div class="col-3 menu-item">
                <a href="all-notice.php">
                    <h4>Notice</h4>
                </a>
            </div>
            <div class="col-3 menu-item">
                <a href="all-research.php">
                    <h4>Research</h4>
                </a>
            </div>
            <div class="col-3 menu-item">
                <a href="results.php">
                    <h4>Results</h4>
                </a>
            </div>
            <div class="col-3 menu-item">
                <a href="contact.php">
                    <h4>Contacts</h4>
                </a>
            </div>
        </div>
        <div class="row menu-extra">
            <div class="col-6 pr-4">
                <div class="row">
                    <div class="col-12 menu-extra-item">
                        <h3>About CMU</h3>
                    </div>
                    <div class="boxx menu-extra-item">
                        <div class="inner-menu-item">
                            <a href="vc-msg.php">
<!--                            <img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pg0KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE5LjAuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPg0KPHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJDYXBhXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4Ig0KCSB2aWV3Qm94PSIwIDAgNTEyIDUxMiIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNTEyIDUxMjsiIHhtbDpzcGFjZT0icHJlc2VydmUiPg0KPGc+DQoJPGc+DQoJCTxwYXRoIGQ9Ik00NjcsNjFINDVDMjAuMjE4LDYxLDAsODEuMTk2LDAsMTA2djMwMGMwLDI0LjcyLDIwLjEyOCw0NSw0NSw0NWg0MjJjMjQuNzIsMCw0NS0yMC4xMjgsNDUtNDVWMTA2DQoJCQlDNTEyLDgxLjI4LDQ5MS44NzIsNjEsNDY3LDYxeiBNNDYwLjc4Niw5MUwyNTYuOTU0LDI5NC44MzNMNTEuMzU5LDkxSDQ2MC43ODZ6IE0zMCwzOTkuNzg4VjExMi4wNjlsMTQ0LjQ3OSwxNDMuMjRMMzAsMzk5Ljc4OHoNCgkJCSBNNTEuMjEzLDQyMWwxNDQuNTctMTQ0LjU3bDUwLjY1Nyw1MC4yMjJjNS44NjQsNS44MTQsMTUuMzI3LDUuNzk1LDIxLjE2Ny0wLjA0NkwzMTcsMjc3LjIxM0w0NjAuNzg3LDQyMUg1MS4yMTN6IE00ODIsMzk5Ljc4Nw0KCQkJTDMzOC4yMTMsMjU2TDQ4MiwxMTIuMjEyVjM5OS43ODd6Ii8+DQoJPC9nPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPC9zdmc+DQo=" />-->
                                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                <h5>Vice Chancellor's Message</h5>
                            </a>
                        </div>
                    </div>
                    <div class="boxx menu-extra-item">
                        <div class="inner-menu-item">
                            <a href="mission-vision.php">
<!--                            <img src="data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjUxMnB0IiB2aWV3Qm94PSIwIDAgNTEyIDUxMiIgd2lkdGg9IjUxMnB0IiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxwYXRoIGQ9Im00NzQuNDE0MDYyIDEyMi40Mzc1IDI0LjgzMjAzMi0yNC44MjgxMjVjNC4xMjUtNC4xMjg5MDYgNS40ODQzNzUtMTAuMjc3MzQ0IDMuNDgwNDY4LTE1Ljc1NzgxMy0yLjAwMzkwNi01LjQ4NDM3NC03LjAwNzgxMi05LjMwNDY4Ny0xMi44MjgxMjQtOS43OTY4NzRsLTQ2LjA2NjQwNy0zLjg4NjcxOS0zLjg4NjcxOS00Ni4wNjY0MDdjLS40ODgyODEtNS44MTY0MDYtNC4zMTI1LTEwLjgyMDMxMi05Ljc5Mjk2OC0xMi44MjQyMTgtNS40ODA0NjktMi4wMDc4MTMtMTEuNjI4OTA2LS42NDg0MzgtMTUuNzU3ODEzIDMuNDgwNDY4bC0yNC44MzIwMzEgMjQuODI4MTI2Yy00MC4xNTYyNS0yNC42MjEwOTQtODYuMDcwMzEyLTM3LjU4NTkzOC0xMzMuNTYyNS0zNy41ODU5MzgtNjguMzc4OTA2IDAtMTMyLjY2Nzk2OSAyNi42Mjg5MDYtMTgxLjAxOTUzMSA3NC45ODA0NjktNDguMzUxNTYzIDQ4LjM1MTU2Mi03NC45ODA0NjkgMTEyLjY0MDYyNS03NC45ODA0NjkgMTgxLjAxOTUzMXMyNi42Mjg5MDYgMTMyLjY2Nzk2OSA3NC45ODA0NjkgMTgxLjAxOTUzMWM0OC4zNTE1NjIgNDguMzUxNTYzIDExMi42NDA2MjUgNzQuOTgwNDY5IDE4MS4wMTk1MzEgNzQuOTgwNDY5czEzMi42Njc5NjktMjYuNjI4OTA2IDE4MS4wMTk1MzEtNzQuOTgwNDY5YzQ4LjM1MTU2My00OC4zNTE1NjIgNzQuOTgwNDY5LTExMi42NDA2MjUgNzQuOTgwNDY5LTE4MS4wMTk1MzEgMC00Ny40OTIxODgtMTIuOTY0ODQ0LTkzLjQwMjM0NC0zNy41ODU5MzgtMTMzLjU2MjV6bS01OS40MTQwNjItMzkuMTI1Yy42MTcxODggNy4yODkwNjIgNi4zOTg0MzggMTMuMDcwMzEyIDEzLjY4NzUgMTMuNjg3NWwyNi41MDM5MDYgMi4yMzQzNzUtMzUuODc1IDM1Ljg3NS0zOS4xMjUtMy4zMDA3ODEtMy4zMDA3ODEtMzkuMTI1IDM1Ljg3NS0zNS44NzV6bS0xMDggMTcyLjY4NzVjMCAyOC4xMjEwOTQtMjIuODc4OTA2IDUxLTUxIDUxcy01MS0yMi44Nzg5MDYtNTEtNTEgMjIuODc4OTA2LTUxIDUxLTUxYzguNjEzMjgxIDAgMTYuNzI2NTYyIDIuMTUyMzQ0IDIzLjg0NzY1NiA1LjkzNzVsLTM0LjQ1MzEyNSAzNC40NTcwMzFjLTUuODU5Mzc1IDUuODU1NDY5LTUuODU5Mzc1IDE1LjM1MTU2MyAwIDIxLjIxMDkzOCAyLjkyNTc4MSAyLjkyOTY4NyA2Ljc2NTYyNSA0LjM5NDUzMSAxMC42MDU0NjkgNC4zOTQ1MzFzNy42Nzk2ODgtMS40NjQ4NDQgMTAuNjA1NDY5LTQuMzk0NTMxbDM0LjQ1NzAzMS0zNC40NTMxMjVjMy43ODUxNTYgNy4xMjEwOTQgNS45Mzc1IDE1LjIzNDM3NSA1LjkzNzUgMjMuODQ3NjU2em0tNS4zNDc2NTYtNjYuODY3MTg4Yy0xMy4wMDc4MTMtOC45MDYyNS0yOC43MzA0NjktMTQuMTMyODEyLTQ1LjY1MjM0NC0xNC4xMzI4MTItNDQuNjY0MDYyIDAtODEgMzYuMzM1OTM4LTgxIDgxczM2LjMzNTkzOCA4MSA4MSA4MSA4MS0zNi4zMzU5MzggODEtODFjMC0xNi45MjE4NzUtNS4yMjY1NjItMzIuNjQ0NTMxLTE0LjEzMjgxMi00NS42NTIzNDRsNDIuODI4MTI0LTQyLjgyODEyNWMxOS41NjY0MDcgMjQuMjEwOTM4IDMxLjMwNDY4OCA1NSAzMS4zMDQ2ODggODguNDgwNDY5IDAgNzcuNzQ2MDk0LTYzLjI1MzkwNiAxNDEtMTQxIDE0MXMtMTQxLTYzLjI1MzkwNi0xNDEtMTQxIDYzLjI1MzkwNi0xNDEgMTQxLTE0MWMzMy40ODA0NjkgMCA2NC4yNjk1MzEgMTEuNzM4MjgxIDg4LjQ4MDQ2OSAzMS4zMDQ2ODh6bTExNC4xNTIzNDQgMjI2LjY3MTg3NmMtNDIuNjgzNTk0IDQyLjY4NzUtOTkuNDM3NSA2Ni4xOTUzMTItMTU5LjgwNDY4OCA2Ni4xOTUzMTJzLTExNy4xMjEwOTQtMjMuNTA3ODEyLTE1OS44MDQ2ODgtNjYuMTk1MzEyYy00Mi42ODc1LTQyLjY4MzU5NC02Ni4xOTUzMTItOTkuNDM3NS02Ni4xOTUzMTItMTU5LjgwNDY4OHMyMy41MDc4MTItMTE3LjEyMTA5NCA2Ni4xOTUzMTItMTU5LjgwNDY4OGM0Mi42ODM1OTQtNDIuNjg3NSA5OS40Mzc1LTY2LjE5NTMxMiAxNTkuODA0Njg4LTY2LjE5NTMxMiAzOS40OTIxODggMCA3Ny43NDYwOTQgMTAuMTQ0NTMxIDExMS42NzE4NzUgMjkuNDc2NTYybC0xNi45MTc5NjkgMTYuOTE3OTY5Yy0yLjczNDM3NSAyLjczNDM3NS00LjI5Mjk2OCA2LjM5ODQzOC00LjM5MDYyNSAxMC4yMjI2NTctLjAxMTcxOS41NDY4NzQuMDAzOTA3IDEuMDkzNzUuMDQ2ODc1IDEuNjQ0NTMxbDIuMDE5NTMyIDIzLjkzMzU5M2MtMjYuNjY3OTY5LTE3LjIwMzEyNC01OC40MDIzNDQtMjcuMTk1MzEyLTkyLjQyOTY4OC0yNy4xOTUzMTItOTQuMjg5MDYyIDAtMTcxIDc2LjcxMDkzOC0xNzEgMTcxczc2LjcxMDkzOCAxNzEgMTcxIDE3MSAxNzEtNzYuNzEwOTM4IDE3MS0xNzFjMC0zNC4wMjczNDQtOS45OTIxODgtNjUuNzYxNzE5LTI3LjE5NTMxMi05Mi40Mjk2ODhsMjMuOTMzNTkzIDIuMDE5NTMyYy40MjE4NzUuMDMxMjUuODQzNzUuMDUwNzgxIDEuMjYxNzE5LjA1MDc4MS4wNzAzMTIgMCAuMTQ0NTMxLS4wMDc4MTMuMjE0ODQ0LS4wMTE3MTkuMTk5MjE4IDAgLjM5NDUzMS0uMDE1NjI1LjU5Mzc1LS4wMjczNDQuMzI0MjE4LS4wMTk1MzEuNjUyMzQ0LS4wMzkwNjIuOTcyNjU2LS4wNzgxMjQuMTk5MjE5LS4wMjczNDQuMzk4NDM4LS4wNTg1OTQuNTk3NjU2LS4wODk4NDQuMzE2NDA2LS4wNTA3ODIuNjI4OTA2LS4xMDkzNzUuOTQxNDA2LS4xNzk2ODguMTk5MjE5LS4wNDY4NzUuMzk0NTMyLS4wOTc2NTYuNTg5ODQ0LS4xNDg0MzcuMzA0Njg4LS4wODIwMzEuNjA1NDY5LS4xNzU3ODEuOTA2MjUtLjI3NzM0NC4xOTUzMTMtLjA2NjQwNi4zODY3MTktLjEzMjgxMy41NzgxMjUtLjIwNzAzMS4yOTI5NjktLjExMzI4Mi41ODIwMzEtLjIzODI4Mi44NzEwOTQtLjM3NS4xODM1OTQtLjA4MjAzMi4zNjcxODctLjE2NDA2My41NTA3ODEtLjI1NzgxMy4yODkwNjMtLjE0ODQzNy41NzAzMTMtLjMxMjUuODQ3NjU2LS40NzY1NjIuMTY3OTY5LS4xMDE1NjMuMzM1OTM4LS4xOTUzMTMuNS0uMjk2ODc1LjI5Njg3Ni0uMTk1MzEzLjU4MjAzMi0uNDA2MjUuODY3MTg4LS42MjEwOTQuMTMyODEyLS4xMDE1NjIuMjY5NTMxLS4xOTE0MDYuMzk4NDM4LS4yOTY4NzUuNDA2MjUtLjMyODEyNS44MDA3ODEtLjY3NTc4MSAxLjE3NTc4MS0xLjA1MDc4MWwxNi45MTc5NjktMTYuOTE3OTY5YzE5LjMzMjAzMSAzMy45MjU3ODEgMjkuNDc2NTYyIDcyLjE3OTY4NyAyOS40NzY1NjIgMTExLjY3MTg3NSAwIDYwLjM2NzE4OC0yMy41MDc4MTIgMTE3LjEyMTA5NC02Ni4xOTUzMTIgMTU5LjgwNDY4OHptMCAwIi8+PC9zdmc+" />-->
                                <i class="fa fa-bullseye" aria-hidden="true"></i>

                                <h5>Vision, Mission & Objective</h5>
                            </a>
                        </div>
                    </div>
                    <div class="boxx menu-extra-item">
                        <div class="inner-menu-item">
                            <a href="syndicate-members.php">
<!--                            <img src="data:image/svg+xml;base64,PHN2ZyBpZD0iTGF5ZXJfMSIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgNTEyLjAwMSA1MTIuMDAxIiBoZWlnaHQ9IjUxMiIgdmlld0JveD0iMCAwIDUxMi4wMDEgNTEyLjAwMSIgd2lkdGg9IjUxMiIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48cGF0aCBkPSJtNDk2Ljc0NSA0NDguOCAxNC40ODYtNDMuNDU2YzEuNTI0LTQuNTc0Ljc1Ny05LjYwMy0yLjA2Mi0xMy41MTQtMi44MTktMy45MTItNy4zNDctNi4yMjktMTIuMTY4LTYuMjI5aC02NS4zMzN2LTIzLjAyN2MxMTUuNjMyLTE0NS4wMDMgOS45ODItMzYzLjI4OS0xNzUuNjctMzYyLjU3Mi0xODUuNjQxLS43Mi0yOTEuMzA5IDIxNy42LTE3NS42NjQgMzYyLjU3MXYyMy4wMjdoLTY1LjMzNGMtNC44MjIgMC05LjM1IDIuMzE4LTEyLjE2OCA2LjIyOS0yLjgxOSAzLjkxMi0zLjU4NiA4Ljk0LTIuMDYyIDEzLjUxNGwxNC40ODYgNDMuNDU3LTE0LjQ4NiA0My40NTdjLTEuNTI0IDQuNTc0LS43NTcgOS42MDMgMi4wNjIgMTMuNTE0IDIuODE5IDMuOTEyIDcuMzQ3IDYuMjI5IDEyLjE2OCA2LjIyOWgxMjguNTMzYzguMjg0IDAgMTUtNi43MTYgMTUtMTV2LTQ5LjI2N2gxOTQuOTMzdjQ5LjI2N2MwIDguMjg0IDYuNzE2IDE1IDE1IDE1aDEyOC41MzRjNC44MjIgMCA5LjM1LTIuMzE4IDEyLjE2OC02LjIyOSAyLjgxOS0zLjkxMiAzLjU4Ni04Ljk0IDIuMDYyLTEzLjUxNHptLTQzNC42MTEtMjI0LjkzM2MwLTEwNi44OTggODYuOTY4LTE5My44NjcgMTkzLjg2Ny0xOTMuODY3IDEzOS43OTQtMS42NTggMjM2LjMzNCAxNTAuMDA4IDE3NS42NjcgMjc1Ljc4NXYtMTcuNjUyYzAtMjEuMDM4LTEwLjM0Mi0zOS42OTUtMjYuMTk5LTUxLjE4OSAyNC4yOTMtMzAuMjc3IDIuMTQ3LTc2LjQ2NS0zNy4wMDItNzYuMjc3LTMxLjg5OC0uNDUxLTU1LjQyNCAzMi43MjEtNDQuNTA5IDYyLjYxMy02Ljg3NS0xMS40MjItMTYuNTUyLTIwLjk2NS0yOC4wNjYtMjcuNyAyNi43NTItMzIuNzkgMi43OTctODMuMzExLTM5Ljg5Mi04My4xMTItNDIuNjg2LS4yLTY2LjY0NiA1MC4zMy0zOS44OTEgODMuMTEyLTExLjUxNCA2LjczNi0yMS4xOTEgMTYuMjc5LTI4LjA2NiAyNy43IDEwLjkxLTI5Ljg5Ni0xMi42MDUtNjMuMDYzLTQ0LjUxLTYyLjYxMy0zOS4xNDUtLjE5LTYxLjI5NyA0Ni4wMDktMzcuMDAxIDc2LjI3Ny0xNS44NTcgMTEuNDk0LTI2LjE5OSAzMC4xNTEtMjYuMTk5IDUxLjE4OXYxNy42NTJjLTExLjg4NS0yNS40ODktMTguMTk5LTUzLjQ2OS0xOC4xOTktODEuOTE4em0zMDYuMzMzIDMxLjA2N2MxOC4zMDcgMCAzMy4yIDE0Ljg5NCAzMy4yIDMzLjJ2MzMuMmgtNjYuNHYtMzMuMmMwLTE4LjMwNyAxNC44OTMtMzMuMiAzMy4yLTMzLjJ6bS0xNy4xMzMtNDcuMTM0YzAtOS40NDcgNy42ODYtMTcuMTMzIDE3LjEzMy0xNy4xMzMgMjIuNzMuOTQxIDIyLjcyMyAzMy4zMjkgMCAzNC4yNjctOS40NDcgMC0xNy4xMzMtNy42ODYtMTcuMTMzLTE3LjEzNHptLTk1LjMzNCA2Ljk2N2MyNy4xNjYgMCA0OS4yNjcgMjIuMTAxIDQ5LjI2NyA0OS4yNjd2NTcuM2gtOTguNTMzdi01Ny4zYzAtMjcuMTY2IDIyLjEwMS00OS4yNjcgNDkuMjY2LTQ5LjI2N3ptLTIxLjE1LTUxLjE1YzAtMTEuNjYyIDkuNDg4LTIxLjE1IDIxLjE1LTIxLjE1IDI4LjA1OCAxLjE2MiAyOC4wNSA0MS4xNDIgMCA0Mi4zLTExLjY2MiAwLTIxLjE1LTkuNDg4LTIxLjE1LTIxLjE1em0tOTEuMzE3IDkxLjMxN2MxOC4zMDcgMCAzMy4yIDE0Ljg5NCAzMy4yIDMzLjJ2MzMuMmgtNjYuNHYtMzMuMmMuMDAxLTE4LjMwNyAxNC44OTQtMzMuMiAzMy4yLTMzLjJ6bS0xNy4xMzMtNDcuMTM0YzAtOS40NDcgNy42ODYtMTcuMTMzIDE3LjEzMy0xNy4xMzMgMjIuNzMuOTQxIDIyLjcyMyAzMy4zMjkgMCAzNC4yNjctOS40NDcgMC0xNy4xMzMtNy42ODYtMTcuMTMzLTE3LjEzNHptMi4xMzMgMjc0LjIwMWgtOTIuNzIxbDkuNDg2LTI4LjQ1N2MxLjAyNi0zLjA3OSAxLjAyNi02LjQwOCAwLTkuNDg3bC05LjQ4Ni0yOC40NTZoNDQuNTIydjE3LjEzM2MwIDguMjg0IDYuNzE2IDE1IDE1IDE1aDMzLjJ2MzQuMjY3em0tMTguMTk5LTY0LjI2N3YtNjYuNGgyOTEuMzMzdjY2LjR6bTM1Ni4zNjkgMzUuODEgOS40ODYgMjguNDU3aC05Mi43MjJ2LTM0LjI2N2gzMy4yYzguMjg0IDAgMTUtNi43MTYgMTUtMTV2LTE3LjEzM2g0NC41MjJsLTkuNDg2IDI4LjQ1NmMtMS4wMjYgMy4wNzktMS4wMjYgNi40MDggMCA5LjQ4N3oiLz48L3N2Zz4=" />-->
                                <i class="fa fa-users" aria-hidden="true"></i>

                                <h5>Syndicate Members</h5>
                            </a>
                        </div>
                    </div>
                    <div class="boxx menu-extra-item">
                        <div class="inner-menu-item">
                            <a href="docs/cmu-act.pdf" target="_blank">
<!--                            <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGhlaWdodD0iNTEycHQiIHZlcnNpb249IjEuMSIgdmlld0JveD0iMCAwIDUxMiA1MTIuMDAwMSIgd2lkdGg9IjUxMnB0Ij4KPGcgaWQ9InN1cmZhY2UxIj4KPHBhdGggZD0iTSAxNSA1MTIgTCAyODcgNTEyIEMgMjk1LjI4NTE1NiA1MTIgMzAyIDUwNS4yODUxNTYgMzAyIDQ5NyBMIDMwMiA0ODEgQyAzMDIgNDY0LjQyOTY4OCAyODguNTcwMzEyIDQ1MSAyNzIgNDUxIEwgMjcyIDQzMy4wNjY0MDYgQyAyNzIgNDA5LjMyMDMxMiAyNTIuNjc5Njg4IDM5MCAyMjguOTMzNTk0IDM5MCBMIDczLjA2NjQwNiAzOTAgQyA0OS4zMjAzMTIgMzkwIDMwIDQwOS4zMjAzMTIgMzAgNDMzLjA2NjQwNiBMIDMwIDQ1MSBDIDEzLjQyOTY4OCA0NTEgMCA0NjQuNDI5Njg4IDAgNDgxIEwgMCA0OTcgQyAwIDUwNS4yODEyNSA2LjcxODc1IDUxMiAxNSA1MTIgWiBNIDE1IDUxMiAiIHN0eWxlPSIgc3Ryb2tlOm5vbmU7ZmlsbC1ydWxlOm5vbnplcm87ZmlsbDpyZ2IoMCUsMCUsMCUpO2ZpbGwtb3BhY2l0eToxOyIgLz4KPHBhdGggZD0iTSAxODcuNzM0Mzc1IDMzMC4zNDM3NSBDIDE5Ny43MDMxMjUgMzQzLjU3NDIxOSAyMTYuNTE1NjI1IDM0Ni4yMTg3NSAyMjkuNzQ2MDk0IDMzNi4yNDYwOTQgQyAyNDIuOTgwNDY5IDMyNi4yNzczNDQgMjQ1LjYyMTA5NCAzMDcuNDY0ODQ0IDIzNS42NTIzNDQgMjk0LjIzMDQ2OSBMIDE0NS4zNzg5MDYgMTc0LjQzNzUgQyAxMzUuNDA2MjUgMTYxLjIwMzEyNSAxMTYuNTk3NjU2IDE1OC41NjI1IDEwMy4zNjcxODggMTY4LjUzMTI1IEMgOTAuMTMyODEyIDE3OC41MDM5MDYgODcuNDg4MjgxIDE5Ny4zMTI1IDk3LjQ2MDkzOCAyMTAuNTQ2ODc1IFogTSAxODcuNzM0Mzc1IDMzMC4zNDM3NSAiIHN0eWxlPSIgc3Ryb2tlOm5vbmU7ZmlsbC1ydWxlOm5vbnplcm87ZmlsbDpyZ2IoMCUsMCUsMCUpO2ZpbGwtb3BhY2l0eToxOyIgLz4KPHBhdGggZD0iTSA0MDMuMzYzMjgxIDE2Ny44NTE1NjIgQyA0MTMuMzM1OTM4IDE4MS4wODIwMzEgNDMyLjE0NDUzMSAxODMuNzI2NTYyIDQ0NS4zNzg5MDYgMTczLjc1MzkwNiBDIDQ1OC42MDkzNzUgMTYzLjc4NTE1NiA0NjEuMjUzOTA2IDE0NC45NzY1NjIgNDUxLjI4MTI1IDEzMS43NDIxODggTCAzNjEuMDA3ODEyIDExLjk0OTIxOSBDIDM1MS4wMzkwNjIgLTEuMjg1MTU2IDMzMi4yMjY1NjIgLTMuOTI5Njg4IDMxOC45OTYwOTQgNi4wNDI5NjkgQyAzMDUuNzY1NjI1IDE2LjAxNTYyNSAzMDMuMTIxMDk0IDM0LjgyNDIxOSAzMTMuMDkzNzUgNDguMDU4NTk0IFogTSA0MDMuMzYzMjgxIDE2Ny44NTE1NjIgIiBzdHlsZT0iIHN0cm9rZTpub25lO2ZpbGwtcnVsZTpub256ZXJvO2ZpbGw6cmdiKDAlLDAlLDAlKTtmaWxsLW9wYWNpdHk6MTsiIC8+CjxwYXRoIGQ9Ik0gMzM3LjU2MjUgMjU1IEMgMzU5LjczNDM3NSAyMzguMjkyOTY5IDM3Ni4zNzg5MDYgMjE3LjU0Njg3NSAzODYuNzg5MDYyIDE5NS43MDMxMjUgTCAyODEuNzUgNTYuMzEyNSBDIDI1Ny44ODI4MTIgNjAuMzAwNzgxIDIzMy4zNTU0NjkgNzAuNTc4MTI1IDIxMS4xNzk2ODggODcuMjg5MDYyIEMgMTg5LjAwNzgxMiAxMDMuOTk2MDk0IDE3Mi4zNjMyODEgMTI0Ljc0MjE4OCAxNjEuOTUzMTI1IDE0Ni41ODIwMzEgTCAyNjYuOTkyMTg4IDI4NS45ODA0NjkgQyAyOTAuODU5Mzc1IDI4MS45OTIxODggMzE1LjM4NjcxOSAyNzEuNzEwOTM4IDMzNy41NjI1IDI1NSBaIE0gMzM3LjU2MjUgMjU1ICIgc3R5bGU9IiBzdHJva2U6bm9uZTtmaWxsLXJ1bGU6bm9uemVybztmaWxsOnJnYigwJSwwJSwwJSk7ZmlsbC1vcGFjaXR5OjE7IiAvPgo8cGF0aCBkPSJNIDM3OC4wNjY0MDYgMjU4LjkwNjI1IEMgMzcxLjA4MjAzMSAyNjYuMDIzNDM4IDM2My43MjY1NjIgMjcyLjg0NzY1NiAzNTUuNjE3MTg4IDI3OC45NjA5MzggQyAzNDcuNTAzOTA2IDI4NS4wNzAzMTIgMzM4LjkxNDA2MiAyOTAuMjYxNzE5IDMzMC4xNTIzNDQgMjk1LjAxNTYyNSBMIDM0OS43MTA5MzggMzIwLjk3MjY1NiBMIDM5Ny42Mjg5MDYgMjg0Ljg2NzE4OCBaIE0gMzc4LjA2NjQwNiAyNTguOTA2MjUgIiBzdHlsZT0iIHN0cm9rZTpub25lO2ZpbGwtcnVsZTpub256ZXJvO2ZpbGw6cmdiKDAlLDAlLDAlKTtmaWxsLW9wYWNpdHk6MTsiIC8+CjxwYXRoIGQ9Ik0gNDE1LjY4MzU5NCAzMDguODI0MjE5IEwgMzY3Ljc2NTYyNSAzNDQuOTI5Njg4IEwgNDU4LjAzOTA2MiA0NjQuNzI2NTYyIEMgNDY4LjAxMTcxOSA0NzcuOTYwOTM4IDQ4Ni44MjAzMTIgNDgwLjYwMTU2MiA1MDAuMDUwNzgxIDQ3MC42MzI4MTIgQyA1MTMuMjg1MTU2IDQ2MC42NjAxNTYgNTE1LjkyNTc4MSA0NDEuODUxNTYyIDUwNS45NTcwMzEgNDI4LjYxNzE4OCBaIE0gNDE1LjY4MzU5NCAzMDguODI0MjE5ICIgc3R5bGU9IiBzdHJva2U6bm9uZTtmaWxsLXJ1bGU6bm9uemVybztmaWxsOnJnYigwJSwwJSwwJSk7ZmlsbC1vcGFjaXR5OjE7IiAvPgo8L2c+Cjwvc3ZnPg==" />-->
                                <i class="fa fa-gavel" aria-hidden="true"></i>

                                <h5>CMU Act</h5>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="col-1">&nbsp;</div> -->
            <div class="col-6 pl-4">
                <div class="row">
                    <div class="col-12 menu-extra-item">
                        <h3>Courses</h3>
                    </div>
                    <div class="boxx menu-extra-item">
                        <div class="inner-menu-item">
                            <a href="single-course.php">

                                <!--                            <img src="data:image/svg+xml;base64,PHN2ZyBpZD0iQ2FwYV8xIiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCA1MTIgNTEyIiBoZWlnaHQ9IjUxMiIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHdpZHRoPSI1MTIiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0ibTUwMS45OTEgMTI4LjM1NC0yNDEtODUuMDMxYy0zLjIyOS0xLjE0LTYuNzUyLTEuMTQtOS45ODEgMGwtMjQxIDg1LjAzMWMtNS45OTIgMi4xMTQtMTAuMDAyIDcuNzc0LTEwLjAxIDE0LjEyOHMzLjk4OSAxMi4wMjMgOS45NzYgMTQuMTUxbDI0MSA4NS42NzdjMS42MjUuNTc4IDMuMzI1Ljg2NyA1LjAyNC44NjcgMS43IDAgMy4zOTktLjI4OSA1LjAyNC0uODY3bDI0MS04NS42NzdjNS45ODctMi4xMjggOS45ODMtNy43OTcgOS45NzYtMTQuMTUxLS4wMDgtNi4zNTQtNC4wMTgtMTIuMDE0LTEwLjAwOS0xNC4xMjh6Ii8+PHBhdGggZD0ibTQ3NS45NzMgMzI4LjU3NHYtMTMwLjg0bC0zMCAxMC42NjV2MTIwLjE3NWMtOS4wMzYgNS4yMDEtMTUuMTI1IDE0Ljk0Ni0xNS4xMjUgMjYuMTIxIDAgMTEuMTc0IDYuMDg5IDIwLjkyIDE1LjEyNSAyNi4xMjF2NzMuNzE2YzAgOC4yODQgNi43MTYgMTUgMTUgMTVzMTUtNi43MTYgMTUtMTV2LTczLjcxNWM5LjAzNi01LjIgMTUuMTI1LTE0Ljk0NyAxNS4xMjUtMjYuMTIxIDAtMTEuMTc1LTYuMDg4LTIwLjkyMS0xNS4xMjUtMjYuMTIyeiIvPjxwYXRoIGQ9Im0yNTYgMjczLjE3N2MtNS4xNDkgMC0xMC4yMi0uODc1LTE1LjA3My0yLjZsLTEzNS40ODMtNDguMTY1djY2LjAwOGMwIDE2LjE0OSAxNi44NDcgMjkuODA2IDUwLjA3MyA0MC41OSAyOC45NjEgOS40IDY0LjY0NyAxNC41NzcgMTAwLjQ4MyAxNC41NzdzNzEuNTIxLTUuMTc3IDEwMC40ODMtMTQuNTc3YzMzLjIyNi0xMC43ODQgNTAuMDczLTI0LjQ0MSA1MC4wNzMtNDAuNTl2LTY2LjAwOGwtMTM1LjQ4MiA0OC4xNjVjLTQuODU0IDEuNzI1LTkuOTI1IDIuNi0xNS4wNzQgMi42eiIvPjwvc3ZnPg==" />-->
                                <i class="fa fa-graduation-cap" aria-hidden="true"></i>

                                <h5>MBBS</h5>
                            </a>
                        </div>
                    </div>
                    <div class="boxx menu-extra-item">
                        <div class="inner-menu-item">
                            <a href="single-course.php">
                                <!--                            <img src="data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjQwMXB0IiB2aWV3Qm94PSItMjIgMCA0MDEgNDAxLjYwNTMzIiB3aWR0aD0iNDAxcHQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0ibTI5NC41ODk4NDQgMTIuNDg0Mzc1Yy00MC4zOTA2MjUtMTMuMTA5Mzc1LTc1LjQ5NjA5NCA1LjkyOTY4Ny05Ni40Njg3NSAxNy4zMDg1OTQtMS44MDA3ODIuOTcyNjU2LTMuNDkyMTg4IDEuODkwNjI1LTUuMDc4MTI1IDIuNzMwNDY5LTIuNzUgMS40NTcwMzEtNi4wOTM3NSAxLjE4NzUtOC41NzQyMTktLjY5MTQwNy0yMC4zMjgxMjUtMTUuNDAyMzQzLTQ0LjAxOTUzMS0zMS44MzIwMzEtNjcuOTcyNjU2LTMxLjgzMjAzMS0yMy4yNjE3MTktLjEwOTM3NS00Ni4wMDc4MTMgNi44NjcxODgtNjUuMjA3MDMyIDIwLTkuMjE4NzUgNi4zNjMyODEtMTcuNDY0ODQzIDE0LjAyNzM0NC0yNC40OTIxODcgMjIuNzUtLjM5NDUzMS43NTc4MTItLjg3ODkwNiAxLjQ2ODc1LTEuNDQxNDA2IDIuMTE3MTg4LTM0LjcxMDkzOCA0NC42NjAxNTYtMzMuMjA3MDMxIDEwNy41NzgxMjQgMy41OTc2NTYgMTUwLjUzMTI1LjIxNDg0NC4yNS40MTQwNjMuNTA3ODEyLjU5Mzc1Ljc4MTI1LjU3MDMxMy44NTU0NjggMS4yODEyNSAxLjg0NzY1NiAxLjk5MjE4NyAyLjg0Mzc1Ljc2NTYyNiAxLjA3NDIxOCAxLjUzNTE1NyAyLjE0ODQzNyAyLjE2NDA2MyAzLjA4NTkzNyAxNi43MTg3NSAyMi4zOTA2MjUgMjQuNDQxNDA2IDUyLjcxMDkzNyAyNi45OTIxODcgNjQuNzMwNDY5IDEuODY3MTg4IDkuMzI4MTI1IDMuNzUgMTkuMzUxNTYyIDUuMDExNzE5IDMwLjA4OTg0NCAzLjUwNzgxMyAzMC4zODI4MTIgOS40MzM1OTQgNTUuODMyMDMxIDE3LjE0MDYyNSA3My42MTcxODcuMDE1NjI1LjAzNTE1Ni4wMjczNDQuMjY1NjI1LjA0Mjk2OS4yOTY4NzUgOCAxOS4yMDMxMjUgMTcuNTc0MjE5IDI5Ljc1NzgxMiAyOC40NjA5MzcgMzAuNzU3ODEyaDIuMDg1OTM4YzQuNDc2NTYyIDAgMTQuMDYyNS01Ljg3MTA5MyAyNy45Njg3NS00My44ODY3MTguMDIzNDM4LS4wNTg1OTQuMDQ2ODc1LS4yMTg3NS4wNjY0MDYtLjI3NzM0NGwxNS4zNDM3NS0zOS4wODU5MzhjNC4wMjczNDQtMTAuNDQxNDA2IDE0LjA4NTkzOC0xNy4zMTI1IDI1LjI4MTI1LTE3LjI2NTYyNC4xNjQwNjMgMCAuMzI4MTI1LS4wMTE3MTkuNS0uMDA3ODEzIDExLjM3MTA5NC4xMzI4MTMgMjEuNDMzNTk0IDcuMzgyODEzIDI1LjE1NjI1IDE4LjEyODkwNmwxMy41NTg1OTQgMzguNTk3NjU3YzExLjc1MzkwNiAzMi4xMjg5MDYgMjIuMTQ0NTMxIDQzLjgwMDc4MSAyNy45Mzc1IDQzLjgwMDc4MWgyLjA3MDMxMmM3LjU4NTkzOC0xIDE4LjU3ODEyNi03IDI4LjQ3NjU2My0zMC43NTc4MTMgNy4yODkwNjMtMTcuMzc4OTA2IDEyLjc1LTQwLjkyMTg3NSAxNy4xOTE0MDYtNzMuODgyODEyIDEuMjU3ODEzLTEwLjY5NTMxMyAzLjE0MDYyNS0yMC43NTc4MTMgNC45ODQzNzUtMjkuOTg0Mzc1IDIuNTc0MjE5LTEyLjE0MDYyNSAxMC4zMzU5MzgtNDIuNjA5Mzc1IDI3LjE0MDYyNS02NS4wMTk1MzEgMjcuODQ3NjU3LTM3LjcwNzAzMiAzNi41NTA3ODEtODUuMTIxMDk0IDIzLjI1LTEyNi43NTc4MTMtOS43MTg3NS0zMS40Mzc1LTMwLjc2OTUzMS01NC4yODEyNS01Ny43NzM0MzctNjIuNzE4NzV6bTAgMCIvPjwvc3ZnPg==" />-->
                                <i class="fa fa-medkit" aria-hidden="true"></i>

                                <h5>BDS</h5>
                            </a>
                        </div>
                    </div>
                    <div class="boxx menu-extra-item">
                        <div class="inner-menu-item">
                            <a href="single-course.php">
                                <!--                            <img src="data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjQ4MHB0IiB2aWV3Qm94PSIwIDAgNDgwIDQ4MCIgd2lkdGg9IjQ4MHB0IiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxwYXRoIGQ9Im0zNzAuMTYwMTU2IDk0LjcxODc1Yy0uNzE4NzUtMi42NDA2MjUtMS4yODEyNS00LjY0MDYyNS0xLjc2MTcxOC02LjIzODI4MWwxNS4wNDI5NjgtMzcuNTE5NTMxYzEuNDQ1MzEzLTMuNzAzMTI2LS4wMTE3MTgtNy45MTAxNTctMy40NDE0MDYtOS45MjE4NzYtNTkuNjc5Njg4LTM0LjA3ODEyNC05OS4xMjEwOTQtNDEuMDM5MDYyLTE0MC00MS4wMzkwNjJzLTgwLjMyMDMxMiA2Ljk2MDkzOC0xNDAgNDEuMDM5MDYyYy0zLjQyOTY4OCAyLjAxMTcxOS00Ljg4NjcxOSA2LjIxODc1LTMuNDQxNDA2IDkuOTIxODc2bDE1LjA0Mjk2OCAzNy41MTk1MzFjLS40ODA0NjggMS41OTc2NTYtMS4wNDI5NjggMy41OTc2NTYtMS43NjE3MTggNi4yMzgyODEtMzEuOTE3OTY5IDExMi4xNjAxNTYtMzcuNjc5Njg4IDE2OC44MDA3ODEtMTguNzE4NzUgMTgzLjYwMTU2MiAyOS42NDQ1MzEgMjEuODY3MTg4IDY0LjM1NTQ2OCAzNS44NTE1NjMgMTAwLjg3ODkwNiA0MC42NDA2MjZ2MjUuMDM5MDYyYzAgNC40MTc5NjkgMy41ODIwMzEgOCA4IDhzOC0zLjU4MjAzMSA4LTh2LTI0LjQ4MDQ2OWM5Ljk2NDg0NCA1LjAxMTcxOSAyMC44NjMyODEgNy44OTg0MzggMzIgOC40ODA0NjkgMTEuMTM2NzE5LS41ODIwMzEgMjIuMDM1MTU2LTMuNDY4NzUgMzItOC40ODA0Njl2MjQuNDgwNDY5YzAgNC40MTc5NjkgMy41ODIwMzEgOCA4IDhzOC0zLjU4MjAzMSA4LTh2LTI1LjAzOTA2MmMzNi41NDI5NjktNC44MDQ2ODggNzEuMjc3MzQ0LTE4Ljc4OTA2MyAxMDAuOTYwOTM4LTQwLjY0MDYyNiAxOC44Nzg5MDYtMTQuODAwNzgxIDEzLjExNzE4Ny03MS40NDE0MDYtMTguODAwNzgyLTE4My42MDE1NjJ6bS00Mi4xNjAxNTYgMTI5LjI4MTI1YzAgNDcuMjgxMjUtNTkuMDM5MDYyIDg4LTg4IDg4cy04OC00MC43MTg3NS04OC04OHYtNDIuNzE4NzVjNjkuNjAxNTYyLTEzLjQ0MTQwNiAxMTEuNTE5NTMxLTM0LjQ4MDQ2OSAxMjYuMzIwMzEyLTQyLjk2MDkzOCA5LjI4MTI1IDEwLjM5ODQzOCAzMC4xNjAxNTcgMzIuMjM4MjgyIDQ5LjY3OTY4OCA0MS40NDE0MDd6bTEyLTEwNy42MDE1NjJjLTMxLjkyMTg3NS0xNi41NTg1OTQtODEuOTIxODc1LTIwLjM5ODQzOC0xMDAtMjAuMzk4NDM4cy02OC4wNzgxMjUgMy44Mzk4NDQtMTAwIDIwLjM5ODQzOGwtMjYtNjQuODc4OTA3YzUzLjE5OTIxOS0yOS4zNTkzNzUgODkuMDM5MDYyLTM1LjUxOTUzMSAxMjYtMzUuNTE5NTMxczcyLjgwMDc4MSA2LjE2MDE1NiAxMjYgMzUuNTE5NTMxem0wIDAiLz48cGF0aCBkPSJtMjU4Ljk5MjE4OCAyNTcuNzY5NTMxYy0xMS4zMTI1IDguMjYxNzE5LTI2LjY3MTg3NiA4LjI2MTcxOS0zNy45ODQzNzYgMC0yLjIxNDg0My0xLjkxNDA2Mi01LjI5Njg3NC0yLjQ2ODc1LTguMDM1MTU2LTEuNDQxNDA2LTIuNzQyMTg3IDEuMDI3MzQ0LTQuNzAzMTI1IDMuNDY4NzUtNS4xMTcxODcgNi4zNjMyODEtLjQxMDE1NyAyLjg5ODQzOC43OTI5NjkgNS43ODkwNjMgMy4xMzY3MTkgNy41MzkwNjMgMTcuMTI1IDEzLjA4OTg0MyA0MC44OTA2MjQgMTMuMDg5ODQzIDU4LjAxNTYyNCAwIDIuMzQzNzUtMS43NSAzLjU0Njg3Ni00LjY0MDYyNSAzLjEzNjcxOS03LjUzOTA2My0uNDE0MDYyLTIuODk0NTMxLTIuMzc1LTUuMzM1OTM3LTUuMTE3MTg3LTYuMzYzMjgxLTIuNzM4MjgyLTEuMDI3MzQ0LTUuODIwMzEzLS40NzI2NTYtOC4wMzUxNTYgMS40NDE0MDZ6bTAgMCIvPjxwYXRoIGQ9Im0zMDQgMjAwYzAgOC44MzU5MzgtNy4xNjQwNjIgMTYtMTYgMTZzLTE2LTcuMTY0MDYyLTE2LTE2IDcuMTY0MDYyLTE2IDE2LTE2IDE2IDcuMTY0MDYyIDE2IDE2em0wIDAiLz48cGF0aCBkPSJtMjA4IDIwMGMwIDguODM1OTM4LTcuMTY0MDYyIDE2LTE2IDE2cy0xNi03LjE2NDA2Mi0xNi0xNiA3LjE2NDA2Mi0xNiAxNi0xNiAxNiA3LjE2NDA2MiAxNiAxNnptMCAwIi8+PHBhdGggZD0ibTIyNCA2NGg4djhjMCA0LjQxNzk2OSAzLjU4MjAzMSA4IDggOHM4LTMuNTgyMDMxIDgtOHYtOGg4YzQuNDE3OTY5IDAgOC0zLjU4MjAzMSA4LThzLTMuNTgyMDMxLTgtOC04aC04di04YzAtNC40MTc5NjktMy41ODIwMzEtOC04LThzLTggMy41ODIwMzEtOCA4djhoLThjLTQuNDE3OTY5IDAtOCAzLjU4MjAzMS04IDhzMy41ODIwMzEgOCA4IDh6bTAgMCIvPjxwYXRoIGQ9Im0yMzIgNDYyLTc0LjMyMDMxMi0yMi4zMjAzMTJjLTMuMzY3MTg4LTEuMDQyOTY5LTUuNjY3OTY5LTQuMTUyMzQ0LTUuNjc5Njg4LTcuNjc5Njg4di0yNy4wMzkwNjJsLTI3LjYwMTU2Mi0xMy44Mzk4NDRjLTEuODk0NTMyLS45NDkyMTktMy4zMzIwMzItMi42MTMyODItMy45OTIxODgtNC42Mjg5MDYtLjY2MDE1Ni0yLjAxMTcxOS0uNDkyMTg4LTQuMjA3MDMyLjQ3MjY1Ni02LjA5Mzc1bDguNjQwNjI1LTE3LjI3NzM0NCA4LjgwMDc4MS0xNy40ODgyODJjLTg5LjI5Njg3NCAxMi4zOTA2MjYtMTM4LjMyMDMxMiA1Ni44NzEwOTQtMTM4LjMyMDMxMiAxMjYuMzY3MTg4IDAgNC40MTc5NjkgMy41ODIwMzEgOCA4IDhoMjI0em0wIDAiLz48cGF0aCBkPSJtMzQxLjc4NTE1NiAzNDUuNzI2NTYyIDguNjk1MzEzIDE3LjQ3MjY1NyA4LjY0MDYyNSAxNy4xOTkyMTljLjk2NDg0NCAxLjg4NjcxOCAxLjEzMjgxMiA0LjA4MjAzMS40NzI2NTYgNi4wOTM3NS0uNjYwMTU2IDIuMDE1NjI0LTIuMDk3NjU2IDMuNjc5Njg3LTMuOTkyMTg4IDQuNjI4OTA2bC0yNy42MDE1NjIgMTMuODM5ODQ0djI3LjAzOTA2MmMtLjAxMTcxOSAzLjUyNzM0NC0yLjMxMjUgNi42MzY3MTktNS42Nzk2ODggNy42Nzk2ODhsLTc0LjMyMDMxMiAyMi4zMjAzMTJ2MThoMjI0YzQuNDE3OTY5IDAgOC0zLjU4MjAzMSA4LTggMC02OS4yOTY4NzUtNDguNzM0Mzc1LTExMy43MjY1NjItMTM4LjIxNDg0NC0xMjYuMjczNDM4em0wIDAiLz48cGF0aCBkPSJtMjMyIDQ0NS4xOTkyMTl2LTMyLjQ4ODI4MWwtNjYuMzIwMzEyLTY2LjM5ODQzOGMtMS41MjczNDQtMS40NzI2NTYtMy41NTg1OTQtMi4zMDA3ODEtNS42Nzk2ODgtMi4zMTI1aC0zLjAzOTA2MmwtOC4yNDIxODggMTYuMzIwMzEyLTEwIDIwLjA3ODEyNiAyNC44ODI4MTIgMTIuNDgwNDY4YzIuNzAzMTI2IDEuMzM5ODQ0IDQuNDEwMTU3IDQuMTAxNTYzIDQuMzk4NDM4IDcuMTIxMDk0djI2LjA3ODEyNXptMCAwIi8+PHBhdGggZD0ibTMzMS4yODEyNSAzNjAuNDgwNDY5LTguMTYwMTU2LTE2LjQ4MDQ2OWgtMy4xMjEwOTRjLTIuMTIxMDk0LjAxNTYyNS00LjE1NjI1Ljg0NzY1Ni01LjY3OTY4OCAyLjMyMDMxMmwtNjYuMzIwMzEyIDY2LjM5MDYyNnYzMi40ODgyODFsNjQtMTkuMTIxMDk0di0yNi4wNzgxMjVjLS4wMTE3MTktMy4wMTk1MzEgMS42OTUzMTItNS43ODEyNSA0LjM5ODQzOC03LjEyMTA5NGwyNC44ODI4MTItMTIuNDgwNDY4em0wIDAiLz48L3N2Zz4=" />-->
                                <i class="fa fa-user-md" aria-hidden="true"></i>

                                <h5>Nursing</h5>
                            </a>
                        </div>
                    </div>
                    <div class="boxx menu-extra-item">
                        <div class="inner-menu-item">
                            <a href="single-course.php">
                                <!--                            <img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pg0KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE5LjAuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPg0KPHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJDYXBhXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4Ig0KCSB2aWV3Qm94PSIwIDAgNDY5LjMzMyA0NjkuMzMzIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA0NjkuMzMzIDQ2OS4zMzM7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxnPg0KCTxnPg0KCQk8cGF0aCBkPSJNMzMwLjY2NywwYy0zNy4wNDIsMC03MS44NzUsMTQuNDI3LTk4LjA0Miw0MC42MTVsLTE5MiwxOTJDMTQuNDM3LDI1OC44MTIsMCwyOTMuNjM1LDAsMzMwLjY2Nw0KCQkJYzAsNzYuNDU4LDYyLjIwOCwxMzguNjY3LDEzOC42NjcsMTM4LjY2N2MzNy4wNjMsMCw3MS44NzUtMTQuNDI3LDk4LjA0Mi00MC42MTVsMTkyLTE5MS45OQ0KCQkJYzI2LjIwOC0yNi4xOTgsNDAuNjI1LTYxLjAyMSw0MC42MjUtOTguMDYzQzQ2OS4zMzMsNjIuMjA4LDQwNy4xMjUsMCwzMzAuNjY3LDB6IE0xNDkuODc1LDIxMy44NzVsLTY0LDYzLjk5DQoJCQlDNzEuNzcxLDI5MS45NjksNjQsMzEwLjcyOSw2NCwzMzAuNjY3YzAsNS44OTYtNC43NzEsMTAuNjY3LTEwLjY2NywxMC42NjdjLTUuODk2LDAtMTAuNjY3LTQuNzcxLTEwLjY2Ny0xMC42NjcNCgkJCWMwLTI1LjY0Niw5Ljk3OS00OS43NSwyOC4xMjUtNjcuODg1bDY0LTYzLjk5YzQuMTY3LTQuMTY3LDEwLjkxNy00LjE2NywxNS4wODMsMA0KCQkJQzE1NC4wNDIsMjAyLjk1OCwxNTQuMDQyLDIwOS43MDgsMTQ5Ljg3NSwyMTMuODc1eiBNMzk4LjU0MiwyMDYuNTYybC04OC40NTcsODguNDQ3TDE3NC4zMzIsMTU5LjI0MWw4OC40Ni04OC40Ng0KCQkJYzE4LjEyNS0xOC4xMzUsNDIuMjI5LTI4LjExNSw2Ny44NzUtMjguMTE1YzUyLjkzOCwwLDk2LDQzLjA2Myw5Niw5NkM0MjYuNjY3LDE2NC4zMTIsNDE2LjY4OCwxODguNDI3LDM5OC41NDIsMjA2LjU2MnoiLz4NCgk8L2c+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8L3N2Zz4NCg==" />-->
                                <i class="fa fa-hospital-o" aria-hidden="true"></i>

                                <h5>Unani</h5>
                            </a>
                        </div>
                    </div>
                    <div class="boxx menu-extra-item">
                        <div class="inner-menu-item">
                            <a href="single-course.php">
                                <!--                            <img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGlkPSJTbWFydF9jb250cm9sIiBkYXRhLW5hbWU9IlNtYXJ0IGNvbnRyb2wiIHZpZXdCb3g9IjAgMCA2NCA2NCIgd2lkdGg9IjUxMiIgaGVpZ2h0PSI1MTIiPjxwYXRoIGQ9Ik00My41ODYsMTYuNTg2YTIsMiwwLDEsMCwyLjgyOCwwQTIsMiwwLDAsMCw0My41ODYsMTYuNTg2WiIvPjxwYXRoIGQ9Ik00OS4yNDMsMTMuNzU3YTYuMDA3LDYuMDA3LDAsMCwwLTguNDg2LDBsMS40MTQsMS40MTRhNC4wMDcsNC4wMDcsMCwwLDEsNS42NTgsMFoiLz48cGF0aCBkPSJNNTIuMDcxLDEwLjkyOWExMCwxMCwwLDAsMC0xNC4xNDIsMGwxLjQxNCwxLjQxNGE4LDgsMCwwLDEsMTEuMzE0LDBaIi8+PHBhdGggZD0iTTUzLjQ4NSw5LjUxNSw1NC45LDguMWExNC4wMTQsMTQuMDE0LDAsMCwwLTE5LjgsMGwxLjQxNCwxLjQxNEExMi4wMTMsMTIuMDEzLDAsMCwxLDUzLjQ4NSw5LjUxNVoiLz48Y2lyY2xlIGN4PSIxOCIgY3k9IjIxIiByPSIyIi8+PHBhdGggZD0iTTEzLjE2LDM3LjYxYy0uMDEsMC0uMDIsMC0uMDItLjAxYTIuNTEsMi41MSwwLDAsMC0uMzEtLjM1bC0uNTQtLjU0YTEuMDE0LDEuMDE0LDAsMCwxLS4xNS0xLjIybDIuMjctMy43OWEuNzg4Ljc4OCwwLDAsMC0uMTItLjk5LjgwOS44MDksMCwwLDAtLjk3LS4xM2wtMS44MSwxLjA4LTEuMzIuOGEyLjAyMywyLjAyMywwLDAsMS0yLjQ1LS4zTDYuNTgsMzFBMi4wMTYsMi4wMTYsMCwwLDEsNiwyOS41OXYtNi40TDQuMDcsMjguMDFhLjk4Ni45ODYsMCwwLDAtLjA3LjM4VjM4LjY3YTMuMDE4LDMuMDE4LDAsMCwwLC42LDEuOGwuNjYuODhBNy4wNyw3LjA3LDAsMCwxLDYuNTcsNDYuN0w1LjEyLDYwaDguNzVMMTIuMTUsNDUuOTFhMi45NjIsMi45NjIsMCwwLDEsLjQ3LTIuMTRsLjcxLTEuMDZBMy45OTMsMy45OTMsMCwwLDAsMTQsNDAuNDl2LS40MmEzLjk4MywzLjk4MywwLDAsMC0uODUtMi40NVoiLz48cGF0aCBkPSJNMjQsMTNIMTJWMjkuMDRsLjI4LS4xN2EyLjg4MywyLjg4MywwLDAsMSwzLjQzLjQyLDIuODE0LDIuODE0LDAsMCwxLC40MiwzLjQ0bC0xLjg3LDMuMTFjLjA1LjA2LjEuMTEuMTUuMTZIMjRaTTE4LDI1YTQsNCwwLDEsMSw0LTRBNCw0LDAsMCwxLDE4LDI1WiIvPjxwYXRoIGQ9Ik0xNS43OSw0MkgyN2ExLDEsMCwwLDAsMS0xVjEwYTEsMSwwLDAsMC0xLTFIOWExLDEsMCwwLDAtMSwxVjI5LjU5bDEuMTYsMS4xNS44NC0uNTFWMTJhMSwxLDAsMCwxLDEtMUgyNWExLDEsMCwwLDEsMSwxVjM3YTEsMSwwLDAsMS0xLDFIMTUuNjJBNi4wNyw2LjA3LDAsMCwxLDE2LDQwLjA3di40MkE1Ljg1NCw1Ljg1NCwwLDAsMSwxNS43OSw0MloiLz48cGF0aCBkPSJNNDYuMDEsNDguMTQsNDQuNzMsMzkuMiw0MC45Nyw1NC4yNGExLjAxMiwxLjAxMiwwLDAsMS0uOTMuNzZINDBhMSwxLDAsMCwxLS45NS0uNjhMMzQuODcsNDEuNzZsLTIuOTYsNi42NUEuOTg3Ljk4NywwLDAsMSwzMSw0OUgyNC44YzUuNTMsNS41OCwxMi42MSw5LjM3LDE0LjIsOS4zNywxLjcxLDAsOC43Ni0zLjg2LDE0LjIyLTkuMzdINDdBMSwxLDAsMCwxLDQ2LjAxLDQ4LjE0WiIvPjxwYXRoIGQ9Ik00OCwyNGExMS45ODIsMTEuOTgyLDAsMCwwLTguMzEsMy4zNSwxLDEsMCwwLDEtMS4zOCwwQTExLjk4MiwxMS45ODIsMCwwLDAsMzAsMjRWNDFhMy4wMDksMy4wMDksMCwwLDEtMywzSDIwLjcyYTI2LjU3LDI2LjU3LDAsMCwwLDIuMjQsM2g3LjM5bDMuNzQtOC40MWExLjAzNiwxLjAzNiwwLDAsMSwuOTYtLjU5LDEsMSwwLDAsMSwuOS42OGwzLjkxLDExLjc1LDQuMTctMTYuNjdBMSwxLDAsMCwxLDQ1LjA1LDMzYTEuMDEyLDEuMDEyLDAsMCwxLC45NC44Nkw0Ny44Nyw0N2g3LjE5QzU3LjkyLDQzLjY1LDYwLDM5Ljg0LDYwLDM2QTEyLjAxLDEyLjAxLDAsMCwwLDQ4LDI0WiIvPjxwYXRoIGQ9Ik0xNyw0N3YyaDUuMDZjLS41OS0uNjUtMS4xNC0xLjMyLTEuNjctMloiLz48cGF0aCBkPSJNNTcuNjQsNDdjLS41MS42OC0xLjA3LDEuMzUtMS42NSwySDYxVjQ3WiIvPjwvc3ZnPgo=" />-->
                                <i class="fa fa-stethoscope" aria-hidden="true"></i>

                                <h5>IMT</h5>
                            </a>
                        </div>
                    </div>
                    <div class="boxx menu-extra-item">
                        <div class="inner-menu-item">
                            <a href="single-course.php">
                                <!--                            <img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pg0KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE5LjAuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPg0KPHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJDYXBhXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4Ig0KCSB2aWV3Qm94PSIwIDAgNTExLjk5OSA1MTEuOTk5IiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MTEuOTk5IDUxMS45OTk7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxnPg0KCTxnPg0KCQk8cGF0aCBkPSJNNDMxLjY4MywwaC0yNzIuMzFjLTQuMTQzLDAtNy41LDMuMzU4LTcuNSw3LjVjMCw0LjE0MiwzLjM1Nyw3LjUsNy41LDcuNWgyNzIuMzFjMy41MTIsMCw2LjM2OSwyLjg1Nyw2LjM2OSw2LjM2OHYxNy4yMDcNCgkJCWMwLDMuNTExLTIuODU3LDYuMzY4LTYuMzY5LDYuMzY4Yy0xMy4yMjcsMC0zMzguMTY5LDAtMzUxLjM2NywwYy0zLjUxMiwwLTYuMzY5LTIuODU3LTYuMzY5LTYuMzY4VjIxLjM2OQ0KCQkJYzAtMy41MTEsMi44NTctNi4zNjgsNi4zNjktNi4zNjhoNTEuMDU1YzQuMTQzLDAsNy41LTMuMzU4LDcuNS03LjVjMC00LjE0Mi0zLjM1Ny03LjUtNy41LTcuNUg4MC4zMTYNCgkJCUM2OC41MzIsMCw1OC45NDUsOS41ODcsNTguOTQ1LDIxLjM2OXYxNy4yMDdjMCwxMC44MDQsOC4yMTYsMjEuMzY5LDIyLjUzOCwyMS4zNjl2MzYwLjE4YzAsMTEuNzgzLDkuNTg3LDIxLjM2OSwyMS4zNywyMS4zNjkNCgkJCWgxNDUuNjQ1djIzLjU3MmMtOS42MDksMy4xNi0xNi41NjYsMTIuMjEyLTE2LjU2NiwyMi44NjRjMCwxMy4yNzEsMTAuNzk3LDI0LjA2NywyNC4wNjcsMjQuMDY3DQoJCQljMTMuMjcxLDAsMjQuMDY2LTEwLjc5NywyNC4wNjYtMjQuMDY3YzAtMTAuNjUzLTYuOTU5LTE5LjcwNS0xNi41NjctMjIuODY0di0yMy41NzJoMTQ1LjY0NWMxMS43ODQsMCwyMS4zNy05LjU4NywyMS4zNy0yMS4zNjkNCgkJCVYxMjguMTgyYzAtNC4xNDItMy4zNTctNy41LTcuNS03LjVzLTcuNSwzLjM1OC03LjUsNy41djI5MS45NDVjMCwzLjUxMS0yLjg1Nyw2LjM2OC02LjM2OSw2LjM2OGgtMzA2LjI5DQoJCQljLTMuNTEyLDAtNi4zNjktMi44NTctNi4zNjktNi4zNjhWNTkuOTQ2aDMxOS4wMjl2NDEuNTY5YzAsNC4xNDIsMy4zNTcsNy41LDcuNSw3LjVzNy41LTMuMzU4LDcuNS03LjVWNTkuOTQ2aDEuMTY4DQoJCQljMTEuNzg0LDAsMjEuMzctOS41ODcsMjEuMzctMjEuMzY5VjIxLjM2OUM0NTMuMDUzLDkuNTg3LDQ0My40NjYsMCw0MzEuNjgzLDB6IE0yNTYsNDc4Ljg2NmM0Ljk5OSwwLDkuMDY2LDQuMDY3LDkuMDY2LDkuMDY3DQoJCQlzLTQuMDY2LDkuMDY3LTkuMDY2LDkuMDY3Yy00Ljk5OSwwLTkuMDY3LTQuMDY3LTkuMDY3LTkuMDY3UzI1MS4wMDEsNDc4Ljg2NiwyNTYsNDc4Ljg2NnoiLz4NCgk8L2c+DQo8L2c+DQo8Zz4NCgk8Zz4NCgkJPHBvbHlnb24gcG9pbnRzPSIyNDMuNjgzLDE2MS40MDIgMjQzLjY4MywxNDIuMjQ3IDI3NC42NDIsMTQyLjI0NyAyNzQuNjQyLDEzMC42NTEgMjQzLjY4MywxMzAuNjUxIDI0My42ODMsMTEyLjk0NiANCgkJCTI3OS41MDcsMTEyLjk0NiAyNzkuNTA3LDEwMC40MTggMjI5LjM5NSwxMDAuNDE4IDIyOS4zOTUsMTczLjkzIDI4MC40NCwxNzMuOTMgMjgwLjQ0LDE2MS40MDIgCQkiLz4NCgk8L2c+DQo8L2c+DQo8Zz4NCgk8Zz4NCgkJPHBvbHlnb24gcG9pbnRzPSIyMjEuNDQ4LDIxOC4zMSAyMjEuNDQ4LDIwNy4zNTkgMTg0Ljc5MiwyMDcuMzU5IDE4NC43OTIsMjYxLjM1NCAxOTcuMjY0LDI2MS4zNTQgMTk3LjI2NCwyNDAuMDYxIA0KCQkJMjE3LjE5LDI0MC4wNjEgMjE3LjE5LDIyOS45NDYgMTk3LjI2NCwyMjkuOTQ2IDE5Ny4yNjQsMjE4LjMxIAkJIi8+DQoJPC9nPg0KPC9nPg0KPGc+DQoJPGc+DQoJCTxwYXRoIGQ9Ik0zMjUuOTAzLDIwOC45NTZjLTQuMTU3LTIuMDU1LTUuMDgyLTEuNTk3LTMwLjE1My0xLjU5N3Y1My45OTVoMTIuNDczdi0xNy40MTZjOS43ODEsMC4wMDEsMjEuNzA4LDIuMTk1LDI2Ljk1OS0xMS40ODMNCgkJCUMzMzguODQ0LDIyMi45MTksMzM0LjA1OCwyMTIuOTg1LDMyNS45MDMsMjA4Ljk1NnogTTMxOC40MTIsMjMzLjA2NGgtMTAuMTlWMjE4LjMxYzEwLjc4MywwLDEwLjQ3OC0wLjEyNCwxMS44NjQsMC40MTgNCgkJCUMzMjUuODI3LDIyMC45OSwzMjQuODQzLDIzMy4wNjQsMzE4LjQxMiwyMzMuMDY0eiIvPg0KCTwvZz4NCjwvZz4NCjxnPg0KCTxnPg0KCQk8cG9seWdvbiBwb2ludHM9IjE1NC45MTcsMzExLjY3NSAxNTQuOTE3LDMxOS44OSAxNjQuODI0LDMxOS44OSAxNjQuODI0LDM0NS45ODcgMTc0LjI0NywzNDUuOTg3IDE3NC4yNDcsMzE5Ljg5IDE4NC4xMDgsMzE5Ljg5IA0KCQkJMTg0LjEwOCwzMTEuNjc1IAkJIi8+DQoJPC9nPg0KPC9nPg0KPGc+DQoJPGc+DQoJCTxwb2x5Z29uIHBvaW50cz0iMzM3LjQ4MiwzMzcuNzcxIDM1Mi41NjEsMzE4LjY4MiAzNTIuNTYxLDMxMS42NzUgMzI2LjM2OCwzMTEuNjc1IDMyNi4zNjgsMzE5Ljg5IDM0MS42ODcsMzE5Ljg5IDMyNS42NDIsMzM4Ljk4IA0KCQkJMzI1LjY0MiwzNDUuOTg3IDM1Mi44NSwzNDUuOTg3IDM1Mi44NSwzMzcuNzcxIAkJIi8+DQoJPC9nPg0KPC9nPg0KPGc+DQoJPGc+DQoJCTxwYXRoIGQ9Ik0yNzIuNTQ4LDMyMi41NDljLTcuMTczLTE4LjI2Ni0zMy42MTEtMTIuMzM3LTMzLjYxMSw2LjIzNGMwLDQuNTQ0LDEuODExLDguODY0LDQuNzgzLDEyLjA1OA0KCQkJQzI1Ni40MjEsMzU0LjQ4MywyNzkuNjIsMzQwLjU4NiwyNzIuNTQ4LDMyMi41NDl6IE0yNDguNTA2LDMyOC44OGMwLTEyLjIxLDE1LjcwNi0xMS45OTUsMTUuNzA2LTAuMDk3DQoJCQlDMjY0LjIxMiwzNDEuMTk0LDI0OC41MDYsMzQwLjg4NywyNDguNTA2LDMyOC44OHoiLz4NCgk8L2c+DQo8L2c+DQo8Zz4NCgk8Zz4NCgkJPHBhdGggZD0iTTM1NC42MjksMjc3LjQ3N0gxNTcuMzcxYy00LjE0MywwLTcuNSwzLjM1OC03LjUsNy41YzAsNC4xNDIsMy4zNTcsNy41LDcuNSw3LjVoMTk3LjI1OGM0LjE0MywwLDcuNS0zLjM1OCw3LjUtNy41DQoJCQlDMzYyLjEyOSwyODAuODM1LDM1OC43NzIsMjc3LjQ3NywzNTQuNjI5LDI3Ny40Nzd6Ii8+DQoJPC9nPg0KPC9nPg0KPGc+DQoJPGc+DQoJCTxwYXRoIGQ9Ik0zNTQuNjI5LDM2Ni44NTlIMTU3LjM3MWMtNC4xNDIsMC03LjUsMy4zNTgtNy41LDcuNXMzLjM1Nyw3LjUsNy41LDcuNWgxOTcuMjU4YzQuMTQzLDAsNy41LTMuMzU4LDcuNS03LjUNCgkJCVMzNTguNzcyLDM2Ni44NTksMzU0LjYyOSwzNjYuODU5eiIvPg0KCTwvZz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjwvc3ZnPg0K" />-->
                                <i class="fa fa-heartbeat" aria-hidden="true"></i>

                                <h5>Optometry</h5>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
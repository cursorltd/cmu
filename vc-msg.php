<?php include('header.php'); ?>
<div class="not-home-page university-home container">
    <div class="vc-msg-area">
        <div class="list-header">
            <h2 class="list-header__header">Vice Chancellor's Message</h2>
             <a href="#" target="_blank" class="list-header__icon list-header__icon--rss list-header__item--mla">
                <span></span>
            </a>
        </div>
        <h4>New frontiers of Health Professional Educations in Bangladesh and Ensuring Academic Quality</h4>
        <div >
        </div>
        <div class="vc-msg">
            <p style="text-align: justify;">
                <img src="images/ViceChancellor.jpg" alt="Vice Chancellor">

                I assume duties as the first Vice Chancellor of Chittagong Medical University on May 13, 2017. The Chittagong Medical University established as per Chittagong Medical University Act, 2016 (Act No 17 of 2016). The vision of the Government of Bangladesh to establish the university is to expand higher education in medical science, enhance the research activities, and maintain quality assurance in both undergraduate and postgraduate Health Professional Educations.
                <br><br>
                I wish to dedicate myself to its vision, prosper lives through education and its mission, and develop globally competent health professionals through our education for a sustainable future, drawing inspirations from our cultural heritage and wisdom.
                <br><br>
                Chittagong Medical University is one of the promising public medical universities, located in Chittagong, the historical port city of Bangladesh. Our aim is to contribute to ongoing institutional and instructional changes in medical education to align with 21st century professional skills which are needed for the society. We are committed on transformative changes in education, so that the future doctors/health professionals can actively contribute on important national and global health issues, including vulnerable subject areas in improving the health and well-being of the population.
                <br><br>
                We have 26 affiliated health professional institutions (Medical/Dental/Nursing/other Allied Health Professional) at present. Our goal is to play the role of enabler of intersection of all institutions/faculty members towards competency-based education, research and care. We believe in collaboration, cooperation and interdependence between institutions towards our mission goals.
                <br><br>
                As we look to the future one thing is certain – knowledge will be a key resource and will be highly sought-after within Bangladesh and around the world. Our challenge is to help to generate ideas that will benefit society, and to educate and train people to work in fields where they will be valued both for their specialized knowledge, and for their ability to research, communicate and solve problems.
                <br><br>
                To meet these challenges we need to build the alliances and collaborative partnerships with other universities and research institutions nationally and globally. This will help to ensure the ongoing relevance of our academic programs and the continued excellence of our teaching, learning and research.
                <br><br>
                Moreover, the eco-friendly greenery campus of Chittagong Medical University, with free-flow of untainted fresh air provides a serene environment for exhilarating academic activity.
                <br><br>
                Last but not least, I forge the motto words for the University “Illuminating the mind by education, research & care”, so all the faculty members, the employees and the students play their active and vibrant role for uplifting this new Medical University to its acme so that she takes a pride of place in the leading medical universities of Bangladesh.
                <br><br>
                We look forward to welcoming you to Chittagong Medical University.
            </p>
        </div>

    </div>

</div>
<?php include('footer.php'); ?>
